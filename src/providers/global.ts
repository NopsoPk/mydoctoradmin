import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

//import { Salon } from '../../providers/salon-interface'

import {
  ToastController,
  LoadingController
} from 'ionic-angular';
import { Loading } from 'ionic-angular/components/loading/loading';

@Injectable()
export class GlobalProvider {
  pushTransitions: NativeTransitionOptions = {
    direction: 'right',
    duration: 3500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  // loader: Loading
  loader: any
  alreadyLoading: boolean = false
  toastAlreadyShowing = false
  DEVICE_DATETIME = 'device_datetime'
  LOGED_IN_SALON_DATA = 'salon_data'
  SALON_TECHS = 'salon_techs'
  LOGED_IN_SALON_OFFERS = 'salon_offers'
  LOGED_IN_SALON_PROFILE_IMG = 'salon_profile_img'

  LOGED_IN_SALON_TECHS_SERVICES = 'techs_services'
  LOGED_IN_SALON_SALON_SETTINGS = 'salon_Settings'
  GCM_TOKEN = 'gcm_toke'

  APP_STATUS_SERVING = 'serving'
  APP_STATUS_FINISHED = 'finished'
  APP_STATUS_ACCEPTED = 'accepted'
  APP_STATUS_REJECTED = 'rejected'
  APP_STATUS_DELETED = 'deleted'
  APP_STATUS_PENDING = 'pending'

  ACCEPT_BUTTON_TEXT = 'Accept'
  REMOVE_BUTTON_TEXT = 'Cancel'
  REJECT_BUTTON_TEXT = 'Reject'
  RESCHEDULE_BUTTON_TEXT = 'Reschedule'

  SUCCESS_STATUS = '1'
  FAILURE_STATUS = '0'
  IS_LOGGED_IN = 'looged_in'
  LOGGED_IN_SALON_ID = 'logged_in_salon_id'
  LOGGED_IN_SALON_NAME = 'logged_in_salon_name'
  LOGGED_IN_SALON_STATUS = 'logged_in_salon_status'
  LOGGED_IN_SALON_ADDRESS = 'logged_in_salon_address'

  SAL_24CLOCK = 'sal_24clock'
  SAL_AUTO_ACCEPT_APP = 'sal_auto_accept_app'
  SAL_FUTURE_APP_DAYS = 'sal_future_app_days'
  NO_MATCH_FOUND = 'No match found.'

  NO_APPOINTMENTS = 'No appointment exists.'
  noInternetMsg = 'You are not connected to internet at the moment, Please try again later.'
  APP_CONFIG_DATA = 'appConfigData'

  LAST_SALONS_DATA_FETCHED_DATE = 'LAST_SALONS_DATA_FETCHED_DATE'
  APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED = 'APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED'

  IS_FORCE_UPDATE_AVAILABLE = 'IS_FORCE_UPDATE_AVAILABLE'
  FORCE_UPDATE_MESSAGE = 'FORCE_UPDATE_MESSAGE'

  constructor(
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public datePipe: DatePipe,
    private nativePageTransitions: NativePageTransitions,

  ) {

    this.loader = this.loadingCtrl.create({
    });
  }

  // CUSTOME FUNCTIONS
  getStringFromDateWithFormat(date, format): string {
    return this.datePipe.transform(date, format)
  }

  AvailableSlotsFor(date: any): string {
    let selectedDate = date.replace(/-/g, '/');
    return this.datePipe.transform(selectedDate, 'dd MMMM');
  }
  getFormattedDate(appointmentTime) {
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (appointmentTime === '0000/00/00 00:00:00') {
      return 'N/A';
    }
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') {
      return this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd (HH:mm)');
    } else { return appointmentTime }
  }

  CalendarDateFormat(appointmentTime) {
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (appointmentTime === '0000/00/00 00:00:00') {
      return 'N/A';
    }
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') {
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd yyyy HH:mm:ss');
    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd yyyy HH:mm');
    } else { return appointmentTime }
  }

  getFormattedSlotTime(time) {
    // 13:30
    const timePart: any[] = time.split(':')
    let timeString = ''

    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') { // 12 hours format
      if (timePart.length === 2) {
        timeString = time
      } else if (timePart.length === 3) {
        timeString = timePart[0] + ':' + timePart[1]
      }
      let hours = timeString.split(':')[0];
      const suffix = Number(hours) >= 12 ? 'PM' : 'AM';
      const hour = ((Number(hours) + 11) % 12 + 1);
      if (hour < 10) {
        return '0' + hour + ':' + time.split(':')[1] + ' ' + suffix;
      }

      return hours = hour + ':' + time.split(':')[1] + ' ' + suffix;

    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      if (timePart.length === 2) {
        return time
      } else if (timePart.length === 3) {
        return timeString = timePart[0] + ':' + timePart[1]
      }
    }
  }

  getFormattedTime(time) {
    // 2018-01-11 11:00:00
    const extractedTime = time.split(' ')[1]
    if (extractedTime === undefined) {
      return time
    }
    const timePart: any[] = extractedTime.split(':')
    let timeString = ''
    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') { // 12 hours format
      if (timePart.length === 2) {
        timeString = extractedTime
      } else if (timePart.length === 3) {
        timeString = timePart[0] + ':' + timePart[1]
      }
      let hours = timeString.split(':')[0];
      const suffix = Number(hours) >= 12 ? 'PM' : 'AM';
      const hour = ((Number(hours) + 11) % 12 + 1);
      if (hour < 10) {
        return '0' + hour + ':' + time.split(':')[1] + ' ' + suffix;
      }

      return hours = hour + ':' + time.split(':')[1] + ' ' + suffix;

    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      if (timePart.length === 2) {
        return extractedTime
      } else if (timePart.length === 3) {
        return timeString = timePart[0] + ':' + timePart[1]
      }
    }
  }

  getCurrentDeviceDateTime(): string {
    const format = 'yyyy-MM-dd HH:mm:ss'
    const currentDate = new Date();

    return this.datePipe.transform(currentDate, format)

  }

  getCurrentDeviceDate(date: string): string {

    const format = 'yyyy-MM-dd'

    let currentDate: Date

    if (date.toString().trim().length === 0) {
      currentDate = new Date()
    } else {
      currentDate = new Date(date);
      if (currentDate === undefined) {
        return ''
      }
    }
    if (currentDate === undefined) {
      return ''
    }
    return this.datePipe.transform(currentDate, format)

  }

  makeToastOnSuccess(message: string) {
    if (this.toastAlreadyShowing) { return false }
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top',
      dismissOnPageChange: false,
      cssClass: 'customToastClassz'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.toastAlreadyShowing = false
    });
    toast.present();
    this.toastAlreadyShowing = true
  }

  makeToastOnFailure(message: string) {
    if (this.toastAlreadyShowing) { return false }
    const toast = this.toastCtrl.create({
      message: message,
      duration: 4000,
      position: 'top',
      dismissOnPageChange: false,
      cssClass: 'failureToastClass'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.toastAlreadyShowing = false
    });


    toast.present();
    this.toastAlreadyShowing = true


  }
  getLoggedInSalonID(): string {
    let loggedInsalon_ID = ''
    loggedInsalon_ID = this.getFromLocalStorage(this.LOGGED_IN_SALON_ID)
    if (loggedInsalon_ID !== undefined) {
      return loggedInsalon_ID
    }
    // let _salon_data = localStorage.getItem(this.LOGED_IN_SALON_DATA)
    // if (_salon_data == undefined) {
    //   return ''
    // }
    // let loggedInSalonData: Salon = JSON.parse(_salon_data)
    // loggedInsalon_ID = loggedInSalonData.sal_id
    // if (loggedInsalon_ID != undefined) {
    //   return loggedInsalon_ID
    // } else {
    //   return loggedInsalon_ID
    // }
  }
  showProgress() {
    if (this.alreadyLoading) { return false }
    console.log('alread Loading: ', this.alreadyLoading);
    console.log('loader started');

    this.loader = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loader.present()
    this.alreadyLoading = true

    if (this.alreadyLoading) {
      setTimeout(() => {
        try {
          console.log('alread Loading: ', this.alreadyLoading);
          if (this.alreadyLoading === true) {
            this.loader.dismiss()
            this.alreadyLoading = false
            console.log(' loader dissmissed in Time out')
          }
          console.log('alread Loading: ', this.alreadyLoading);
        }
        catch (Exception) {
          console.log('cannot dismiss loader ');
        }
      }, 15000);
    }

  }
  stopProgress() {
    if (!this.alreadyLoading) { return false }
    try {
      console.log('alread Loading: ', this.alreadyLoading);
      console.log('loader dismissed');
      this.loader.dismiss()
      this.alreadyLoading = false
    }
    catch (Exception) {
      console.log('cannot dismiss loader ');
    }
  }

  setInLocalStorage(key: string, Value: any) {
    localStorage.setItem(key, JSON.stringify(Value))
  }
  getFromLocalStorage(key: string): any {
    if (!key) return ''
    const obj = localStorage.getItem(key)
    if (obj !== undefined && obj !== 'undefined' && obj !== null) {
      return JSON.parse(obj)
    } else {
      return ''
    }

  }
  removeFromStorageForTheKey(obj: string) {
    localStorage.removeItem(obj);
  }
  nativePagePushTransition() {
    return this.nativePageTransitions.slide(this.pushTransitions)
  }

  getStaticService(){
  let service="consultancy";
    
    return service;
  }
}
