import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SharedataProvider {

  makeAppointmentSource: BehaviorSubject<any> = new BehaviorSubject('');
  loggedInSalonData: BehaviorSubject<any> = new BehaviorSubject('');
  
  techAppointmentComplete: BehaviorSubject<any> = new BehaviorSubject('');
  updateNotificationsData: BehaviorSubject<any> = new BehaviorSubject('');
  techIndex: BehaviorSubject<any> = new BehaviorSubject('');
  
  
  
  public currencySymbol?= ''
  public distance_unit?= ''
  public privacy_policy? = ''
  public showCalenderView=true
  constructor() {

  }

}
