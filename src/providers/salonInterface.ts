export interface Salon {
  sal_id?: string,
  sal_name?: string,
  sal_address?: string,
  sal_city?: string,
  sal_zip?: string,
  sal_contact_person?: string,
  sal_email?: string,
  sal_phone?: string,
  sal_hours?: string,
  sal_pic?: string,
  sal_profile_pic?: string,
  sal_created_datetime?: string,
  sal_status?: Boolean,
  sal_password?: string,
  admin_device_id?: string,
  admin_device_type?: string,
  sal_lat?: string,
  sal_lng?: string,
  is_active?: Boolean,
  sal_auto_accept_app?: string,
  sal_future_app_days?: string,
  sal_24clock?: Boolean,
  sal_queue_status?: Boolean,
  sal_specialty?: '',
  sal_facebook?: string,
  sal_instagram?: string,
  sal_reviews?: string,
  sal_rating?: string,
  sal_modify_datetime?: string,
  sal_timing?: string,
  sal_website?: string,
  sal_nearby_zip?: string,
  sal_active_promotions?: '1',
  sal_is_default?: Boolean,
  sal_search_words?: string,
  sal_biography?: string,
  services?: Services[],
  categories?: Category[],
  offers?: Offers[],
  queue?: [{}],
  pending_appointments_count?: number,
  sal_techs?: Sal_Techs[],
  sal_services: Services[],
  service_categories: Category[],
}

export interface Services {
  sser_id: number,
  ser_id: number,
  sser_name: string,
  sal_id: number,
  sser_rate: string,
  sser_time: string,
  sser_enabled: Boolean,
  sser_featured: Boolean,
  ssc_id: number,
  sser_order: number,
  ssc_name: string,
  checked: Boolean,

}

export interface Category {
  ssc_id: number,
  ssc_name: string,
  sal_id: number,
  ssc_is_active: Boolean,
  services: Services[],

}

export interface Offers {
  so_id: number,
  so_title: string,
  sot_id: number,
  so_amount: number,
  so_notification: string,
  so_notification_type: string,
  so_is_active: Boolean,
  sal_id: number,
  so_days?: string,
  so_expiry?: string,
  so_time?: string,
}

export interface Day_slot {
  slot: number[];
}

export interface Sal_Techs {
  sal_id: number,
  tech_id: number,
  tech_name: string,
  tech_pic: string,
  tech_slots: Day_slot[],
  tech_phone: string,
  tech_specialty: string,
  tech_bio: string,
  tech_status: Boolean,
  tech_is_active: number,
  tech_off: number,
  categories: Category[],
  tech_services: Services[],
  tech_appointments: Tech_appointments[],
  tech_fee:Number,
  isSelected?: boolean
}
export interface Tech_appointments {
  app_id: string,
  app_created: string,
  app_services: string,
  app_price: string,
  app_est_duration: string,
  app_act_start_time: string,
  app_act_end_time: string,
  app_act_duration: string,
  app_start_time: string,
  start_time: string,
  app_end_time: string,
  app_type: string,
  app_slots: string,
  app_status: string,
  app_out_scheduled: number,
  sal_id: number,
  cust_id: string,
  tech_id: number,
  app_rating: null,
  app_review: string,
  app_review_datetime: string,
  app_notes: string,
  app_review_hide: number,
  so_id: number,
  app_price_without_offer: string,
  app_user_info: string,
  reminded_24hr: string,
  reminded_1hr: string,
  app_last_modified: string,
  cust_name: string,
  cust_pic: string,
  cust_phone: string,
  cust_zip: string,
  showDetails: Boolean,
  icon: String,
}

export interface PrevApp {
  app_id?: String;
  app_start_time?: String;
  tech_name?: String;
  tech_pic?: String;
  app_notes?: String;
  parent_id?: String;
}

export interface Settings {
  status: number
}

export interface Notifications {
  app_id: string,
  app_created: string,
  app_services: string,
  app_price: string,
  app_est_duration: string,
  app_act_start_time: string,
  app_act_end_time: string,
  app_act_duration: string,
  app_start_time: string,
  app_end_time: string,
  app_type: string,
  app_slots: string,
  app_status: string,
  app_out_scheduled: string,
  sal_id: string,
  cust_id: string,
  tech_id: string,
  app_rating: number,
}
export interface Add_New_Customer {

  status?: string,
  cust_id?: string,
  cust_pic_name: string,
  sms_message: string,
  sms_status: string,

}
export interface Tech_Slots {

  app_id: string,
  duration_ahead: string,
  tech_id: string,
  ts_date: string,
  ts_duration: string,
  ts_end_time: string,
  ts_id: string,
  ts_is_booked: string,
  ts_number: string,
  ts_start_time: string,
}
export interface Customer {
  cust_id: string
  cust_name: string,
  cust_pic: string,
  cust_zip: string,
  cust_phone: string,
  cust_datetime: string,
  cust_device_id: string,
  lat: string,
  lng: string,
  cust_device_type: string,
  cust_credits: string,
  referrer_code: string,
  auth_code_expiry: string,
  sal_id: string,
  cust_email: string,
}
export interface PageInterface {
  title: string;
  pageName: string;
  component?: any;
  index?: number;
  icon: string;
}
export interface APP_CONFIG { 
    
  app_path?:string,
  client_api_path?:string, 
  admin_api_path?:string, 
  cust_initial_credit?: string, 
  environment?: string, 
  scraping_interval?:string, 
  last_fetch_id?:string, 
  last_exported_pin?: string, 
  search_radius?: string, 
  currency?: string, 
  distance_unit?: string,
  privacy_policy?: string,
  
}
