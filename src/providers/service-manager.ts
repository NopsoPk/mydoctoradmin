import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { config } from './config'

@Injectable()
export class ServiceManager {

  headers: Headers
  options: RequestOptions

  constructor(public http: Http) {
    this.setUpHeaderAndOptions()
  }

  getData(params) {
    
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log('zahid');
    
    console.log(config.ApiUrl + percentEncodedJsonData);

    return this.http.post(config.ApiUrl + percentEncodedJsonData, this.options)
      .map(Response => Response.json());

  }
  // Setting up head and options
  setUpHeaderAndOptions() {
    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Access-Control-Allow-Methods', 'GET,POST')
    this.headers.append('Access-Control-Allow-Headers', 'Authorization, Content-Type')

    this.options = new RequestOptions({ headers: this.headers });
  }
}
