import { PinImageViewerPage } from './../pages/pin-image-viewer/pin-image-viewer';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { DatePipe } from '@angular/common';
import { EmailComposer } from '@ionic-native/email-composer';
import { FCM } from '@ionic-native/fcm';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { DashboardPage }     from '../pages/dashboard/dashboard'
import { AppointmentsPage }  from '../pages/appointments/appointments'
import { NotificationsPage } from '../pages/notifications/notifications'
import { SettingsPage } from '../pages/settings/settings';
import { PrivacyPolicyPage} from '../pages/privacy-policy/privacy-policy'
import { FinishAppPage } from '../pages/finish-app/finish-app';
import { OffersPage } from '../pages/offers/offers';

import { NotesPage } from '../pages/notes/notes';
import {ContactUsPage } from '../pages/contact-us/contact-us'
import {TermsCondPage } from '../pages/terms-cond/terms-cond'

import { SearchCustomerPage } from '../pages/search-customer/search-customer';
import { AddNewCustomerPage } from '../pages/add-new-customer/add-new-customer';
import { AddAppointmentPage } from '../pages/add-appointment/add-appointment';
import { SlotsViewPage } from '../pages/slots-view/slots-view';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalProvider } from '../providers/global';
import { ServiceManager } from '../providers/service-manager';
import {CalendarComponent} from '../components/calendar/calendar'
import {AppointmentDetailComponent} from '../components/appointment-detail/appointment-detail'
import { PrevAppComponent } from '../components/prev-app/prev-app'
import { AppointmentsPageDetailComponent } from '../components/appointments-page-detail/appointments-page-detail'
import { SharedataProvider } from '../providers/sharedata/sharedata';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';

import { Geolocation } from '@ionic-native/geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { AppInfoPage } from '../pages/app-info/app-info';
import { IonicStorageModule } from '@ionic/storage';
// import { Diagn/ostic } from "@ionic-native/diagnostic";
import { Diagnostic } from '@ionic-native/diagnostic';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ForceUpdatePage } from '../pages/force-update/force-update';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    FinishAppPage,
    OffersPage,
    NotesPage,
    HomePage,
    TabsPage,
    DashboardPage,
    AppointmentsPage,
    NotificationsPage,
    SettingsPage,
    PrivacyPolicyPage,
    ContactUsPage,
    SearchCustomerPage,
    AddNewCustomerPage,
    AddAppointmentPage,
    SlotsViewPage,
    TermsCondPage,
    PinImageViewerPage,
    LoginPage,CalendarComponent,
    AppointmentDetailComponent,
    PrevAppComponent,
    AppointmentsPageDetailComponent,
    AppInfoPage,
    ForceUpdatePage,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    IonicSwipeAllModule,
    IonicModule.forRoot(MyApp,{
      scrollPadding: false,
      scrollAssist: false
    }),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DashboardPage,
    AppointmentsPage,
    NotificationsPage,
    SettingsPage,
    PrivacyPolicyPage,
    ContactUsPage,
    SearchCustomerPage,
    FinishAppPage,
    NotesPage,
    OffersPage,
    AddNewCustomerPage,
    TermsCondPage,
    AddAppointmentPage,
    SlotsViewPage,
    PinImageViewerPage,
    LoginPage, CalendarComponent,
    AppInfoPage,
    ForceUpdatePage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,
    ServiceManager,
    DatePipe,
    EmailComposer,
    FCM,
    AppVersion,
    Device,
    Network,
    SharedataProvider,
    NativePageTransitions,
    
    Geolocation,
    Keyboard,
    Diagnostic,
    AndroidPermissions,
  ]
})
export class AppModule {}
