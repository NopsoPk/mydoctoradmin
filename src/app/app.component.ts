import { Component, ViewChild } from '@angular/core';
import { App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { EmailComposer } from '@ionic-native/email-composer';
import { Platform, Nav } from 'ionic-angular';
import { SharedataProvider } from '../providers/sharedata/sharedata'

import { PageInterface, APP_CONFIG } from '../providers/salonInterface'
import { AppointmentsPage } from '../pages/appointments/appointments';
import { NotificationsPage } from '../pages/notifications/notifications';
import { SettingsPage } from '../pages/settings/settings';
import { TermsCondPage } from '../pages/terms-cond/terms-cond';

import { GlobalProvider } from '../providers/global'
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { ContactUsPage } from '../pages/contact-us/contact-us';

import { FCM } from '@ionic-native/fcm';

import { Events } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { config } from '../providers/config';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Geolocation } from '@ionic-native/geolocation';
import { ServiceManager } from '../providers/service-manager';
import { AppMessages } from '../providers/AppMessages/AppMessages';
// import { DiagnosticOriginal } from '@ionic-native/diagnostic';
import { Diagnostic } from '@ionic-native/diagnostic';
import { timer } from 'rxjs/observable/timer';
import { AppVersion } from '@ionic-native/app-version';
import { ForceUpdatePage } from '../pages/force-update/force-update';
import { areIterablesEqual } from '@angular/core/src/change_detection/change_detection_util';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  isLogin: string;
  salonProfileIconPath = '';
  logoutSource = 'assets/imgs/logout2.png'
  locationSource = 'assets/imgs/location.png'

  rootPage: any = null;
  salonName = '';
  salonPic = '';

  message: string = '';
  @ViewChild(Nav) nav: Nav;
  pages: Array<{ title: string, component: any, index: number, icon: string }>;

  constructor(
    public events: Events,
    private fcm: FCM,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public global: GlobalProvider,
    public emailComposer: EmailComposer,
    protected app: App,
    public network: Network,
    private sharedProvider: SharedataProvider,
    private nativePageTransitions: NativePageTransitions,
    private geolocation: Geolocation,
    public serviceManager: ServiceManager,
    private diagnostic: Diagnostic,
    public alertCtrl: AlertController,
    public appVersion: AppVersion,

  ) {
    this.initializeApp();
    this.setRoot()

  }
  public isSalonloggedIn = false
  public _isForceUpdateAvailable = false;
  setRoot() {
    this.platform.is('android') ? this.appPlatform = 'android' : this.appPlatform = 'ios'

    this._isForceUpdateAvailable = this.global.getFromLocalStorage(this.global.IS_FORCE_UPDATE_AVAILABLE)
    this.isSalonloggedIn = this.global.getFromLocalStorage(this.global.IS_LOGGED_IN);
    this.isSalonloggedIn = Boolean(this.isSalonloggedIn)

    if (!this.isSalonloggedIn) {
      this.rootPage = LoginPage;
    } else if (!this._isForceUpdateAvailable) {
      
      this.rootPage = TabsPage;
      timer(4000).subscribe(() => {
        this.platform.ready().then(() => {
          this.getAppVersionAndcheckForceUpdate()
        })
      })
    } else if (this._isForceUpdateAvailable) {
      // alert(this.isSalonloggedIn + ' 98')
      this.platform.ready().then(() => {
        this.getAppVersionAndcheckForceUpdate()
      })
    }

  }

  initFcm() {

    this.fcm.subscribeToTopic('marketing');

    this.fcm.onNotification().subscribe(data => {
      this.events.publish('NeedToUpdate', true);
      if (data.wasTapped) {
        this.gotToNotifications()
        console.log("Received in background");
      } else {
        console.log("Received in foreground");
      };
    })

    if (this.global.getFromLocalStorage(this.global.GCM_TOKEN)) {

      return
    }
    this.fcm.getToken().then(token => {
      if (token) {
        this.global.setInLocalStorage(this.global.GCM_TOKEN, token);
      }
    });

    this.fcm.onTokenRefresh().subscribe((token) => {
      if (token) {
        this.global.setInLocalStorage(this.global.GCM_TOKEN, token)
      }
    })

  }

  checkConnection() {
    let disconnectSubscription = this.network.onDisconnect().subscribe((data) => {
      console.log('network was disconnected', data);
      this.global.makeToastOnFailure(AppMessages.msgInternetIsDisconnected);
    });

    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe((data) => {
      console.log('network connected!', data);
      this.global.makeToastOnSuccess(AppMessages.msgInternetIsConnected);

    });
  }

  openPage(page: PageInterface) {

    let params = {};
    if (page.index) {
      params = { tabIndex: page.index }
    }

    if (page.index !== undefined) {
      switch (page.index) {
        case 0:
          this.nav.getActiveChildNav().select(0);
          break;
        case 1:
          this.nav.getActiveChildNav().select(1);
          break;
        case 2:
          this.nav.getActiveChildNav().select(2);
          break
        case 3:
          this.nav.getActiveChildNav().select(3);
          break

        case 4:
          this.nav.push(TermsCondPage);
          break
        case 5:
          this.nav.push(PrivacyPolicyPage);

          break
        case 6:
          this.openEmail();
          break
        default:
          break
      }
    }

  }
  openEmail() {
    let email = {
      to: 'contactus@DoctorApp.Pk',
      isHtml: true
    };

    this.emailComposer.open(email)
  }

  logOut() {
    this.logoutUserFromServer(this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID), this.global.getFromLocalStorage(this.global.GCM_TOKEN))
    localStorage.clear()
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_OFFERS, null);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_TECHS_SERVICES, null);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_DATA, null);
    this.global.setInLocalStorage(this.global.SALON_TECHS, null);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_SALON_SETTINGS, null);
    this.global.setInLocalStorage(this.global.IS_LOGGED_IN, null);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_PROFILE_IMG, null);
    this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_NAME, null);
    this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_ADDRESS, null);
    this.global.setInLocalStorage(this.global.SAL_FUTURE_APP_DAYS, null);
    this.global.setInLocalStorage(this.global.SAL_24CLOCK, null);
    this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_STATUS, null);
    this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_ID, null);
    this.global.GCM_TOKEN
    this.global.setInLocalStorage(this.global.GCM_TOKEN, null);
    localStorage.clear()
    this.fcm.unsubscribeFromTopic('marketing').then(res => {
      this.initFcm()

    })
    const nav = this.app.getRootNav();
    nav.setRoot(LoginPage);
    // this.rootPage = LoginPage;

  }
  updateLocationTapped = false;
  updatelocation() {
    this.updateLocationTapped = true;
    this.openLocationSettingIfNeeded()
  }

  gotToNotifications() {
    let transitionOptions: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };

    let nav = this.app.getActiveNav();
    let view = nav.getActive().instance.pageName;
    if (view == 'FinishAppPage') {
      nav.pop()
    }

    this.nav.popToRoot()
    this.nativePageTransitions.slide(transitionOptions)
    this.nav.getActiveChildNav().select(2)
    return
    /** 

    let options = {
      abc: 'cde'
    }

    let isHomePageInStack = false
    for (let i = 0; i < this.app.getRootNav().length(); i++) {
      let v = this.app.getRootNav().getViews()[i];
      if (!isHomePageInStack && v.component.name === 'TabsPage') {
        isHomePageInStack = true
      }

    }
    if (isHomePageInStack) {
      this.nativePageTransitions.slide(transitionOptions)
      if (this.app.getRootNav().length === 1) {
        alert('if first')
        this.nav.getActiveChildNav().select(2)
      } else {
        alert('if second ' + this.nav.getAllChildNavs().length)
        // this.app.getActiveNavs().pop()
        this.nav.popToRoot()
        // this.nav.setRoot(TabsPage, {
        //     opentab: 2 
        // });
        // this.app.getActiveNav().parent.select(2);
        this.nav.getActiveChildNav().select(2)
        // this.gsp.tabIndex = "1";
      }

    } else {
      // this.app.getActiveNav().popToRoot();
      // this.app.getRootNav().popToRoot()
      // this.nav.popAll()
      // this.rootPage = TabsPage
      alert('else else ')
      this.nav.popToRoot()
      this.nav.setRoot(TabsPage, {
        opentab: 2
      });
      // this.nav.getActiveChildNav().select(2)
      // this.app.getActiveNav().push(TabsPage, {
      //   opentab: 2 
      // }).then(this.nav.getActiveChildNav().select(2)) ;

      // for (let i = 0; i < this.navCtrl.length(); i++) {
      //   let v = this.navCtrl.getViews()[i];
      //   console.log(v.component.name);

      // }
    }
*/
  }

  getAndSaveAppConfig() {
    // if (this.global.getFromLocalStorage(this.global.APP_CONFIG_DATA))return 

    const params = {
      service: btoa('get_config'),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          let appConfigData: APP_CONFIG = res.config
          this.global.setInLocalStorage(this.global.APP_CONFIG_DATA, appConfigData)
          this.sharedProvider.currencySymbol = appConfigData.currency
          this.sharedProvider.distance_unit = appConfigData.distance_unit
          this.sharedProvider.privacy_policy = appConfigData.privacy_policy
        },
        (error) => {
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        }
      )

  }

  logoutUserFromServer(sal_id, token) {
    const params = {
      service: btoa('logout'),
      sal_id: btoa(sal_id),
      device_id: btoa(token),

    }

    // this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          // this.global.makeToastOnSuccess(res.msg, 'top')
        },
        (error) => {

          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        },
        () => {
          this.global.stopProgress()
        }
      )
  }

  initializeApp() {

    this.platform.ready().then(() => {
      this.initFcm();

      this.ionViewWillEnter()
      // this.platform.registerBackButtonAction(() => { });
      this.statusBar.backgroundColorByHexString('#000000'); // set status bar to black
      this.statusBar.styleLightContent();

      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
      this.getAndSaveAppConfig()
    });
  }
  ionViewWillEnter() {

    this.pages = [
      { title: 'Today', component: TabsPage, index: 0, icon: 'assets/imgs/dashboard.png' },
      { title: 'Appointments', component: AppointmentsPage, index: 1, icon: 'assets/imgs/appointment.png' },
      { title: 'Messages', component: NotificationsPage, index: 2, icon: 'assets/imgs/notifications.png' },
      { title: 'Settings', component: SettingsPage, index: 3, icon: 'assets/imgs/settings.png' },
      { title: 'Terms & Conditions', component: TermsCondPage, index: 4, icon: 'assets/imgs/terms@3x.png' },
      { title: 'Privacy Policy', component: PrivacyPolicyPage, index: 5, icon: 'assets/imgs/privacy@3x.png' },
      { title: 'Contact Us', component: ContactUsPage, index: 6, icon: 'assets/imgs/contact.png' },

    ];

    this.sharedProvider.loggedInSalonData.subscribe((res) => {
      console.log('logged in menu data', JSON.stringify(res));

      if (res) {

        this.salonProfileIconPath = ''
        this.salonProfileIconPath = config.salonImgUrl;
        this.salonName = res.salonName;
        // this.salonPic = res.salonPic;
        if (this.global.getFromLocalStorage('zahidz')) {
          this.salonProfileIconPath += this.global.getFromLocalStorage('zahidz')
        } else {
          this.salonProfileIconPath += 'default.png'
        }
        // alert(this.salonProfileIconPath);

      }
    });

    this.checkConnection();

  }
  openLocationSettingIfNeeded() {
    this.platform.ready().then(() => {
      if (this.platform.is('android')) {
        this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {
          this.updateLocationTapped = false;
          isLocationEnabled ? this.getCurrenctLocation() : this.confirmSwitchToSetting()
        }, er => {
          this.updateLocationTapped = false;
        })
        if (this.updateLocationTapped) {
          this.platform.resume.subscribe(() => {
            this.updateLocationTapped = false;
            this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {

              isLocationEnabled ? this.getCurrenctLocation() : this.confirmSwitchToSetting()
            })

          });
        }

      } else {
        this.getCurrenctLocation()
      }
    })//platform ready

  }
  gotLocationGoigToUpdateCustomeLocation = false
  gpsOptions = { maximumAge: 300000, timeout: 10000, enableHighAccuracy: true };
  latitude: number
  longitude: number
  getCurrenctLocation() {

    if (this.gotLocationGoigToUpdateCustomeLocation) {
      // alert('gonig back cause already got location and pushed to server')
      return
    }

    this.geolocation.getCurrentPosition(this.gpsOptions).then(location => {
      // alert('got location')
      this.latitude = location.coords.latitude
      this.longitude = location.coords.longitude
      this.gotLocationGoigToUpdateCustomeLocation = true;
      this.updateCustomerLocationOnServer()
    }, onError => {

      // alert(JSON.stringify(onError))
      // alert(JSON.stringify(onError.message))
    })
  }
  updateCustomerLocationOnServer() {
    const salID = this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID)
    const params = {
      service: btoa('update_location'),
      sal_id: btoa(salID),
      sal_lat: btoa(this.latitude.toString()),
      sal_lng: btoa(this.longitude.toString()),
    }

    // this.global.showProgress()

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          this.global.makeToastOnSuccess(res.msg)
          this.gotLocationGoigToUpdateCustomeLocation = false
        },
        (error) => {
          // this.global.stopProgress()
          console.log(error);

          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        },
        () => {
          this.global.stopProgress()
        }
      )

  }
  confirmSwitchToSetting() {
    if (this.platform.is('android')) {
      let alert = this.alertCtrl.create({
        title: 'Device location request',
        message: 'Your device location service is not enabled for BeautyApp.  Please allow BeautyApp to access your location.',
        buttons: [
          {
            text: 'Deny',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');

            }
          },
          {
            text: 'Allow',
            handler: () => {
              console.log('Yes clicked');

              this.diagnostic.switchToLocationSettings()
            }
          }
        ]
      });
      alert.present().then(() => {

      });
    }

  }

  public appInfo = ''
  getAppVersionAndcheckForceUpdate() {

    this.appInfo = ''
    try {
      this.appVersion.getAppName().then(appName => {
        this.appName = appName
        this.appInfo += 'app_Name: ' + appName + ',\n'
        this.appVersion.getPackageName().then(packageName => {
          this.appInfo += ('package_Name: ' + packageName + ',\n')
          this.appVersion.getVersionCode().then(buildNo => {
            this.buildNo = buildNo
            this.appInfo += ('version Code: ' + buildNo + ',\n')
            this.appVersion.getVersionNumber().then(versionNumber => {
              this.Version = versionNumber
              this.appInfo += ('versionNumber: ' + versionNumber + ',\n')
              this.checkForceUpdate()

            })
          })
        })
      })
    } catch (error) {

    }

  }
  public appPlatform = ''
  public Version = ''
  public buildNo = ''
  public appName = ''
  public showForceUpdatePushed = false;
  checkForceUpdate() {

    // if (this.global.getFromLocalStorage(this.global.APP_CONFIG_DATA))return 
    // this.buildNo = '324'
    // this.Version = '1.1.2'
    // alert('going')
    const params = {
      service: btoa('force_update'),
      platform: btoa(this.appPlatform),
      app_version: btoa(this.Version),
      app_build_no: btoa(this.buildNo),
      app_name: btoa(this.appName)
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          if (res.force_update && Number(res.force_update) === 1) {
            this.global.setInLocalStorage(this.global.IS_FORCE_UPDATE_AVAILABLE, true)
            this.global.setInLocalStorage(this.global.FORCE_UPDATE_MESSAGE, res.update_msg)
            this.app.getRootNav().setRoot(ForceUpdatePage, {
              update_msg: res.update_msg
            })
          } else {
            this.rootPage = TabsPage;
            this.global.removeFromStorageForTheKey(this.global.IS_FORCE_UPDATE_AVAILABLE)
          }
        },
        (error) => {
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        }
      )
  }
}
