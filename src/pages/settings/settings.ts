import { Component } from '@angular/core';
import { ServiceManager } from '../../providers/service-manager'
import { GlobalProvider } from '../../providers/global'
import { SearchCustomerPage } from '../search-customer/search-customer';
import { Keyboard } from '@ionic-native/keyboard';
import {
  IonicPage, NavController, NavParams,
  ModalController,
  LoadingController,
  Events
} from 'ionic-angular';

import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { AppMessages } from '../../providers/AppMessages/AppMessages';


@IonicPage()
@Component({
  selector: 'app-page-settings',
  templateUrl: 'settings.html'
})

export class SettingsPage {

  valueforngif = true;
  pushTransitions: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };


  loading: any
  futureAppoDays = ''
  toggleAutoAccept: boolean
  toggletwentyFourHours: boolean
  testValue: string

  constructor(public modalController: ModalController,
    public keyBoard: Keyboard,
    public nativePageTransitions: NativePageTransitions,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public serviceManager: ServiceManager,
    public events: Events,
    public global: GlobalProvider) {

    this.loading = this.loadingCtrl.create({
    });

  }

  ionViewDidEnter() {
    
    this.keyBoard.onKeyboardShow().subscribe(() => { this.valueforngif = false })
    this.keyBoard.onKeyboardHide().subscribe(() => { this.valueforngif = true })
  }
  ionViewDidLoad() {
    const fapodays = this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS)
    if (fapodays === undefined) {
      this.global.makeToastOnFailure(AppMessages.msgMissingSalonsSettings)
      return
    }
    this.futureAppoDays = fapodays
    this.toggletwentyFourHours = false
    if (this.global.getFromLocalStorage(this.global.SAL_24CLOCK) === '1') {
      this.toggletwentyFourHours = true
    }

    this.toggleAutoAccept = false
    if (this.global.getFromLocalStorage(this.global.SAL_AUTO_ACCEPT_APP) === '1') {
      this.toggleAutoAccept = true
    }

  }
  ionViewWillEnter() {
    this.events.publish('setTabIndex', 3);
    // this.checkConnection()
  }

  btnMakeAppo() {
    this.nativePageTransitions.slide(this.pushTransitions)
    this.navCtrl.push(SearchCustomerPage, {})
    // this.global.makeToastOnSuccess('You can start on Making appo', 'top')

  }

  btnSubmitTapped() {
    let abc = Number(this.futureAppoDays.toString().trim())

    if (abc === undefined || abc <= 0) {
      this.global.makeToastOnFailure('Please set future appointment great than 1')
      return
    }

    let _autoAccept = ''
    let _24HoursFormat = ''
    if (this.toggleAutoAccept === true) {
      _autoAccept = '1'
    } else {
      _autoAccept = '0'
    }
    this.toggletwentyFourHours === true ? _24HoursFormat = '1' : _24HoursFormat = '0'
    const params = {
      service: btoa('update_settings'),
      sal_id: btoa(this.global.getLoggedInSalonID()),
      sal_auto_accept_app: btoa(_autoAccept),
      sal_24clock: btoa(_24HoursFormat),
      sal_future_app_days: btoa(this.futureAppoDays),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime())
    }

    this.global.showProgress()
    this.serviceManager.getData(params)
    .retryWhen((err) => {
      return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
              return retryCount;
          }
          else {
              throw (err);
          }
      }, 0).delay(1000)
  })  
      .subscribe(
      (resAppStatus) => {
        if (resAppStatus.status === 1 || resAppStatus.status === '1') {
          this.global.makeToastOnSuccess(AppMessages.msgSettingsUpdation);
          this.global.setInLocalStorage(this.global.LOGED_IN_SALON_SALON_SETTINGS, params)

          if (atob(params.sal_24clock) === '1') {
            this.toggletwentyFourHours = true
          } else {
            this.toggletwentyFourHours = false
          }
          if (atob(params.sal_auto_accept_app) === '1') {
            this.toggleAutoAccept = true
          } else {
            this.toggleAutoAccept = false
          }
          this.global.setInLocalStorage(this.global.SAL_FUTURE_APP_DAYS, this.futureAppoDays);
          let fapodays = this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS)
          this.futureAppoDays = atob(params.sal_future_app_days)
          console.log(atob(params.sal_auto_accept_app));
        }
      },
      (error) => {
        this.global.stopProgress()
        this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        console.log('something went wrong', error);
      },
      () => {
        this.global.stopProgress()
      }
      );
    console.log('Submit button tapped')
  }

}
