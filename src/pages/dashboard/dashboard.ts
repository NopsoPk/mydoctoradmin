import { Component, OnInit, ViewChild } from '@angular/core';
import { App, IonicPage, NavController, NavParams, LoadingController, ModalController, Slides, Alert, AlertController, Content } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager';
import { Salon, Sal_Techs, Offers, Tech_appointments, Services, Category } from '../../providers/salonInterface';
import { GlobalProvider } from '../../providers/global'
import { SharedataProvider } from '../../providers/sharedata/sharedata'
import { SearchCustomerPage } from '../search-customer/search-customer';
import { config } from '../../providers/config';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { Observable } from "rxjs/Observable";
import { Subscription } from 'rxjs/Subscription';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Platform } from 'ionic-angular/platform/platform';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { AppInfoPage } from '../app-info/app-info';
import { AppVersion } from '@ionic-native/app-version';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { timer } from 'rxjs/observable/timer';
import { SlotsViewPage } from '../slots-view/slots-view';
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  baseUrl = config.techImg;
  HideonScroll = false;
  heightOfCalendar = 100;
  calImgURL = '../../assets/imgs/calView.png'
  salonServicesCategories: Salon;
  sal_techs: Sal_Techs[] = [];
  selectedTech: Sal_Techs;
  times = [];
  salonSubscription: Subscription;
  appointments: Tech_appointments[] = [];
  techServices: Services[];
  serviceCategories: Category[] = [];
  offers: Offers[];
  Serving = true;
  calenderview = false;
  activetech = false;
  salonData: Salon[] = []
  selectedTechIndex = 0;
  appointmentDetail = '';
  appStartTime: string;
  currentDate: string;
  techimgUrl: string;
  custimgUrl: string;
  toggleicon = 'add';
  toggle = false;
  breakTimeY = 0;
  breakTimeHeight = 0;
  pixelsPerMinute = 4
  shouldGetTechServices = true
  shouldShowCalender = true
  public alertShown: boolean = false
  public isFirstLaunchOfApp: boolean
  public SalonFetchedData: any
  public unregisterBackButtonAction: any;
  public appInfo = ''
  public myAppName = ''
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private serviceManager: ServiceManager,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private sharedProvider: SharedataProvider,
    public global: GlobalProvider,
    public appCtrl: App,
    public network: Network,
    public nativePageTransitions: NativePageTransitions,
    public platForm: Platform,
    public alertCtrl: AlertController,
    public appVersion: AppVersion,
    public diagnostic: Diagnostic,
    public geolocation: Geolocation
  ) {
    this.myAppName = config.AppName
  }
  ngOnInit() {
    this.stopSubscription();
  }
  ionViewCanEnter() {
    console.log('can enter');
  }
  ionViewWillLeave() {
    this.stopSubscription();
  }
  ionViewWillEnter() {
    this.showForceUpdatePushed = false
    this.getAppVersion()
    this.subscribeToData()
    let _isFirstLaunchOfApp = this.global.getFromLocalStorage('isFirstLaunchOfApp')
    let isNumberOne = Number(_isFirstLaunchOfApp)
    isNumberOne === 1 ? this.isFirstLaunchOfApp = false : this.isFirstLaunchOfApp = true
    this.showOldSalonLikeSkeleton()
    this.stopSubscription();
    this.sharedProvider.makeAppointmentSource.subscribe((res) => {
      // console.log('appointment completed', res);
      if (res) {
        console.log('timer called....');
        this.getsalons(true)
        this.getAppointments(this.sal_techs[this.selectedTechIndex], this.selectedTechIndex);
      }
    });
    // this.sharedProvider.techIndex.subscribe((res) => {
    //   console.log('appointment completed of tech index', res);
    //   if (res) {
    //     this.selectedTechIndex = res;
    //      console.log('appointment completed of tech index', res);
    //     //  this.getAppointments(res, res.tech_id );
    //      this.getAppointments(this.sal_techs[res], res);         
    //   }
    // });
    // this.getsalons(true)
    console.log('will enter');
  }
  onScroll(event) {
    console.log(event);
    this.HideonScroll = true;
    //   let scrollposition = event.additionalEvent;
    // console.log('Direction', scrollposition);
    // if (scrollposition === 'panup') {
    //   console.log('now show');
    //   this.HideonScroll = true;
    //   // console.log('this.selectedTech', this.selectedTech);
    // } else {
    //   this.HideonScroll = false;
    //   console.log('now hide');
    // }
  }
  /** zahid 
   * body will effect soul and soul effect body
   * sicnkess of people all the time: radion/ virbation 
   * no nature thing, people are not taking care of body
   * 
   * there are more and more illness everyday and it's increasign day by day
   * people are getting sick from eating but it was suppose to help to live to get nuerients
   * why is that
   * @param event body
   */
  swipedown(event) {
    console.log(event);
    this.HideonScroll = false;
  }
  ionViewDidLoad() {
    this.initializeBackButtonCustomHandler();
    this.techimgUrl = config.techImg;
    this.custimgUrl = config.custImg;
    this.currentDate = this.global.getCurrentDeviceDate('');
    for (let i = 0; i < this.navCtrl.length(); i++) {
      let v = this.navCtrl.getViews()[i];
      console.log(v.component.name);
    }
  }
  selectedEnviroment: number
  ionViewDidEnter() {
    this.platForm.is('android') ? this.appPlatform = 'android' : this.appPlatform = 'ios'
    this.selectedEnviroment = this.global.getFromLocalStorage('zahidzahidzahid')
    if (!this.selectedEnviroment) this.selectedEnviroment = 1
    // alert(this.selectedEnviroment)
    this.shouldShowCalender = true
    // for (let i = 0; i < this.navCtrl.length(); i++) {
    //   let v = this.navCtrl.getViews()[i];
    //   alert(v.component.name);
    //   console.log(v.component.name);
    //   alert()
    // }
    if (this.isFirstLaunchOfApp) {
      timer(4000).subscribe(() => {
        this.confirmToUpdateLocation()
      })
    }
  }
  createTimesArray() {
    console.log("Times mehtod...");
    let salHours = this.salonData['sal_hours'].split(',');
    let todaysHours;
    if (new Date().getDay() != 0) {
      todaysHours = salHours[new Date().getDay() - 1];
      let splittedHours = todaysHours.split('&');
      let startHour = splittedHours[0].split(':')[0];
      let startMinute = splittedHours[0].split(':')[1];
      let endHour = splittedHours[1].split(':')[0];
      let endMinute = splittedHours[1].split(':')[1];
      this.createSlots(startHour, startMinute, endHour, endMinute);
    } else {
    }
  }
  public salonIsOff = false
  createSlots(startHour, startMinute, endHour, endMinute) {
    console.log('hour' + startHour + 'Minute' + startMinute + 'hour' + endHour + 'minute' + endMinute)
    if (startHour > endHour) {
      this.global.makeToastOnFailure(AppMessages.msgSalonTimeConfiguration);
      this.shouldShowCalender = false
      return;
    } else if (startHour == endHour) {
      this.salonIsOff = true
      // alert("Salon is Off.");
      // this.global.makeToastOnFailure(AppMessages.msgOffSalon);
      return;
    } else {
      this.salonIsOff = false
      let slot = 1;
      this.heightOfCalendar = (endHour - startHour) * 60 * 4;
      this.times = [];
      for (let i = startHour; i < endHour; i++) {
        for (let j = 0; j < 60; j++) {
          if (j == 0) {
            this.times.push((('0' + i).slice(-2)) + ":00");
          }
          else {
            this.times.push((('0' + i).slice(-2)) + ":" + (('0' + j * slot).slice(-2)));
          }
        }
      }
      // this.times.push((endHour) + ":00");
      if (endMinute > 0) {
        for (let k = 0; k < endMinute; k++) {
          this.times.push(endHour + ":" + (('0' + k * slot).slice(-2)))
        }
      }
      if (startMinute > 0) {
        for (let k = 0; k < startMinute; k++) {
          this.times.shift();
        }
      }
    }
    this.calculateBreakTime(this.selectedTechIndex);
  }
  calculateBreakTime(tech_index) {
    if (!this.sal_techs || this.sal_techs.length == 0) {
      return;
    }
    console.log('this.selectedTechIndex', tech_index);
    let tempTech = this.sal_techs[tech_index];
    console.log('selected Tech', tempTech);
    let breakTime = []
    if (tempTech['tech_break_time'] !== undefined) {
      breakTime = tempTech['tech_break_time'].split(',')
    } else {
      this.global.makeToastOnFailure(AppMessages.msgSalonTimeConfiguration);
      return;
    }
    console.log('Break time => ', breakTime)
    let dayOfTheWeekk = new Date().getDay()
    console.log('day of the week =>', dayOfTheWeekk)
    if (dayOfTheWeekk == 1) {
      dayOfTheWeekk = 6
    } else {
      dayOfTheWeekk -= 1
    }
    console.log('day of the week =>', dayOfTheWeekk)
    if (breakTime[dayOfTheWeekk] === undefined) {
      return;
    }
    let breakStart = breakTime[dayOfTheWeekk].split('&')[0]
    let breakEnd = breakTime[dayOfTheWeekk].split('&')[1]
    if (breakStart === undefined) {
      return;
    }
    if (breakEnd === undefined) {
      return;
    }
    if (breakStart != breakEnd) {
      let breakStartIndex = this.times.indexOf(breakStart)
      let breakEndIndex = this.times.indexOf(breakEnd)
      if (breakStartIndex != -1 && breakEndIndex != -1) {
        this.breakTimeHeight = (breakEndIndex - breakStartIndex) * this.pixelsPerMinute
        this.breakTimeY = breakStartIndex * this.pixelsPerMinute;
        // this.getAppointments(this.sal_techs[this.selectedTechIndex], 0);
      }
    }
  }
  //   if (tempTech['tech_break_time'] !== undefined) { breakTime = tempTech['tech_break_time'].split('&'); }
  //   console.log('tempTech', breakTime);
  //   this.breakTimeY = this.getTimeFromSlot(breakTime[0]) * 4;
  //   this.breakTimeHeight = (breakTime[1] - breakTime[0]) * 20;
  //   this.getAppointments(this.sal_techs[this.selectedTechIndex], 0);
  // }
  getTimeFromSlot(slot) {
    return this.times.indexOf(this.times[(slot - 1) * 5]);
  }
  getIndexFromTime(time) {
    return this.times.indexOf(time);
  }
  getAppoHeight(appo) {
    return appo.app_est_duration * 4;
  }
  getMarginTop(appo) {
    return -(this.heightOfCalendar - this.getIndexFromTime(appo.app_start_time.split(' ')[1].substr(0, 5)) * 4);
  }
  // gotoSlide(tech: Sal_Techs, index) {
  //   this.selectedTechIndex = index;
  //   let currentIndex = this.selectedTechIndex;
  //   console.log('Current index is', currentIndex);
  //   this.slides.slideTo(currentIndex, 500);
  // }
  getAppointments(tech: Sal_Techs, index) {
    this.selectedTech = tech;
    this.selectedTechIndex = index;
    this.calculateBreakTime(this.selectedTechIndex);
    this.activetech = true;
    // console.log('this.activetech', this.activetech);
    this.sal_techs.forEach((tech, MyIndex) => {
      if (index === MyIndex) return tech.isSelected = true;
      else return tech.isSelected = false;
    })
    console.log('this.activetech', this.selectedTech);
    console.log('this.activetech index', this.selectedTechIndex);
    // console.log('get appo user', user);
    let myAppos = tech.tech_appointments
    if (myAppos && myAppos !== null && myAppos.length > 0 && myAppos !== undefined) {
      const appointments = [...myAppos];
      console.log('appointments...appointments');
      console.log(JSON.stringify(appointments));
      this.appointments = []
      this.appointments = appointments.filter(function (myAppo) {
        return myAppo.app_status !== 'finished' && myAppo.app_status !== 'pending' && myAppo.app_status !== 'rejected' && myAppo.app_status !== 'deleted'
      })
      if (this.appointments.length > 0) {
        this.appointments.forEach((element) => {
          element.icon = 'add';
        });
      }
    } else {
      this.appointments = []
    }
    this.techServices = tech.tech_services;
    console.log('Appointments', this.appointments);
  }
  callToCust(cust_phone: string) {
    window.open('tel:' + cust_phone);
  }
  calenderswitch() {
    console.log('calendaer value', this.calenderview);
    if (this.shouldShowCalender === true) {
      this.calenderview = !this.calenderview;
    } else {
      this.global.makeToastOnFailure(AppMessages.msgSalonTimeConfiguration);
    }
    console.log('calendaer value', this.calenderview);
  }
  refreshAppointment(appointment) {
    this.getsalons(true);
    this.getAppointments(this.sal_techs[this.selectedTechIndex], this.selectedTechIndex);
  }
  ngOnDestroy() {
    this.stopSubscription();
  }
  private stopSubscription() {
    if (this.salonSubscription) {
      this.salonSubscription.unsubscribe();
    }
  }
  private subscribeToData() {
    this.salonSubscription = Observable.timer(50000).first().subscribe(() => {
      this.getsalons(false)
    });
  }
  showOldSalonLikeSkeleton() {
    this.SalonFetchedData = this.global.getFromLocalStorage('SalonFetchedData')
    // this.SalonFetchedData = this.global.getFromLocalStorage('resresresres')
    if (this.SalonFetchedData) {
      let res = this.SalonFetchedData
      this.global.setInLocalStorage('SalonFetchedData', res)
      console.log('just called services');
      this.offers = res.salon.offers;
      this.salonData = res.salon;
      this.serviceCategories = res.salon.service_categories;
      this.sal_techs = res.salon.sal_techs;
      if (!this.selectedTech && this.sal_techs) {
        if (this.sal_techs && this.sal_techs.length >= 3) {
          this.selectedTechIndex = 1
          this.selectedTech = this.sal_techs[1]
        } else {
          this.selectedTechIndex = 0
          this.selectedTech = this.sal_techs[0]
        }
      } else if (this.selectedTech) {
        let jobDone = false
        let TestTech = []
        // let ohNo: Sal_Techs
        // let _index
        TestTech.push(this.selectedTech)
        this.sal_techs.forEach((tech, index) => {
          if (Number(tech.sal_id) === Number(this.selectedTech.tech_id)) {
            this.selectedTechIndex = index
            return tech.isSelected = true;
          } else {
            return tech.isSelected = false
          }
        });
        // this.sal_techs.every(function (Tech, index) {
        //   if (Tech.tech_id === TestTech[0].tech_id) {
        //     ohNo = Tech
        //     jobDone = true
        //     _index = index
        //     return false
        //   } else return true
        // })
        // this.selectedTech = ohNo
        // this.selectedTechIndex = _index
        // if (!jobDone) {
        //   this.sal_techs.forEach((myTech, i) => {
        //     if (Number(myTech.tech_status) === 1) {
        //       this.selectedTechIndex = i
        //       this.selectedTech = myTech
        //     }
        //   });
        // }
      }
      console.log('__sal_tech', this.sal_techs);
      if (this.sal_techs !== null && this.selectedTechIndex <= this.sal_techs.length && this.sal_techs[this.selectedTechIndex] !== undefined && this.sal_techs[this.selectedTechIndex] !== null) {
        this.techServices = this.sal_techs[this.selectedTechIndex].tech_services;
        this.getAppointments(this.sal_techs[this.selectedTechIndex], this.selectedTechIndex);
        this.createTimesArray();
      } else {
        this.global.makeToastOnFailure(AppMessages.msgTechnicianMissing)
        console.log('no tech found');
      }
      this.saveDataInLocalStorage(res.salon);
    }
  }
  public appPlatform = ''
  public Version = ''
  public buildNo = ''
  public appName = ''
  public showForceUpdatePushed = false
  getsalons(isShow: boolean) {
    this.isFirstLaunchOfApp ? isShow = true : isShow = false;
    const salID = this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID)
    if (salID === undefined) {
      this.global.makeToastOnFailure(AppMessages.msgSalonIdMissing)
      return
    }
    let _sal_last_fetched_data = this.global.getFromLocalStorage(this.global.LAST_SALONS_DATA_FETCHED_DATE)
    if (!_sal_last_fetched_data) _sal_last_fetched_data = ''
    // this.buildNo = '324'
    const params = {
      service: btoa('sal_servs_techs'),
      platform: btoa(this.appPlatform),
      app_version: btoa(this.Version),
      app_build_no: btoa(this.buildNo),
      app_name: btoa(this.appName),
      sal_id: btoa(salID),
      tech_services: btoa('1'),//active services = 1, all services(active & inactive)
      last_fetched: btoa(_sal_last_fetched_data),
      app_date: btoa(this.global.getCurrentDeviceDate('')),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
      tech_is_active: btoa('1')
    }
    console.log('stopProgress @ 260 dasboard');
    if (!_sal_last_fetched_data || _sal_last_fetched_data.trim().length === 0) {
      this.global.showProgress()
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        this.global.stopProgress()
        this.global.setInLocalStorage(this.global.LAST_SALONS_DATA_FETCHED_DATE, res.response_datetime)
        if (res && Number(res.is_modified) === 1) {
          this.global.setInLocalStorage('SalonFetchedData', res)
          this.global.setInLocalStorage('isFirstLaunchOfApp', true)
          this.offers = res.salon.offers;
          this.salonData = res.salon;
          this.serviceCategories = res.salon.service_categories;
          this.sal_techs = res.salon.sal_techs;
          if (!this.selectedTech && this.sal_techs.length > 0) {
            if (this.sal_techs && this.sal_techs.length >= 3) {
              this.selectedTechIndex = 1
              this.selectedTech = this.sal_techs[1]
            } else {
              this.selectedTechIndex = 0
              this.selectedTech = this.sal_techs[0]
            }
          } else if (this.selectedTech) {
            let jobDone = false
            let TestTech = []
            let ohNo: Sal_Techs
            let _index
            TestTech.push(this.selectedTech)
            this.sal_techs.every(function (Tech, index) {
              if (TestTech && Tech.tech_id === TestTech[0].tech_id && Number(Tech.tech_status) === Number(TestTech[0].tech_status)) {
                ohNo = Tech
                jobDone = true
                _index = index
                return false
              } else return true
            })
            this.selectedTech = ohNo
            this.selectedTechIndex = _index
            if (!jobDone) {
              this.sal_techs.forEach((myTech, i) => {
                if (Number(myTech.tech_status) === 1) {
                  this.selectedTechIndex = i
                  this.selectedTech = myTech
                }
              });
            }
          }
          this.subscribeToData();
          console.log('__sal_tech', this.sal_techs);
          if (this.sal_techs !== null && this.selectedTechIndex <= this.sal_techs.length && this.sal_techs[this.selectedTechIndex] !== undefined && this.sal_techs[this.selectedTechIndex] !== null) {
            this.techServices = this.sal_techs[this.selectedTechIndex].tech_services;
            this.getAppointments(this.sal_techs[this.selectedTechIndex], this.selectedTechIndex);
            this.createTimesArray();
          } else {
            this.global.makeToastOnFailure(AppMessages.msgTechnicianMissing)
            console.log('no tech found');
          }
          this.saveDataInLocalStorage(res.salon);
        } else {
          this.subscribeToData()
        }
        this.content.scrollTo(0, 1).then(res => {
          this.content.resize()
          this.content.scrollTo(0, 0, 0).then(res => {
          })
        })
      },
        error => {
          this.global.stopProgress()
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  rescheduleAppointment(appointment: Tech_appointments) {
    console.log('appointment', JSON.stringify(appointment))
    this.nativePageTransitions.slide(this.options)
    this.appCtrl.getRootNav().push(SlotsViewPage, {
      appointment: appointment,
      isRescheduleAppointment: true,
      techIndex: this.selectedTechIndex,
      selected_Tech: this.selectedTech,
      selected_TechIndex: this.selectedTechIndex,
      appo_TotalTime: 10,
      appo_TotalPrice: appointment.app_price,
      appoServicesString: appointment.app_services,
      cust_id: appointment.cust_id,
      isReschedule: true,
    })
  }
  openMenu() {
    let logged_in_Salon = {
      salonName: this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_NAME),
      salonPic: this.global.getFromLocalStorage(this.global.LOGED_IN_SALON_PROFILE_IMG),
    }
    this.sharedProvider.loggedInSalonData.next(logged_in_Salon)
  }
  private saveDataInLocalStorage(res: Salon) {
    let fapodays = this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS)
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_PROFILE_IMG, res.sal_profile_pic);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_OFFERS, this.offers);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_TECHS_SERVICES, this.techServices);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_DATA, this.salonData);
    this.global.setInLocalStorage(this.global.SALON_TECHS, this.sal_techs);
    this.global.setInLocalStorage(this.global.SAL_24CLOCK, res.sal_24clock);
    this.global.setInLocalStorage(this.global.SAL_AUTO_ACCEPT_APP, res.sal_auto_accept_app);
    if (!fapodays) {
      this.global.setInLocalStorage(this.global.SAL_FUTURE_APP_DAYS, res.sal_future_app_days);
    }
  }
  btnMakeAppointment() {
    this.nativePageTransitions.slide(this.options)
    this.appCtrl.getRootNav().push(SearchCustomerPage, { selected_Tech: this.selectedTech });
  }
  getFromattedTime(appointmentTime) {
    return this.global.getFormattedTime(appointmentTime)// this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
  }
  getFromattedDateTime(appointmentTime) {
    return this.global.CalendarDateFormat(appointmentTime)// this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
  }
  get12HourFormatFrom24Hour(time) {
    let hours = time.split(':')[0];
    var suffix = Number(hours) >= 12 ? "PM" : "AM";
    var hour = ((Number(hours) + 11) % 12 + 1);
    if (hour < 10) {
      return "0" + hour + ':' + time.split(':')[1] + ' ' + suffix;
    }
    return hours = hour + ':' + time.split(':')[1] + ' ' + suffix;
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platForm.registerBackButtonAction(() => {
      if (this.alertShown == false) {
        this.alertShown = true
        let alert = this.alertCtrl.create({
          title: 'Confirm you want to exit....',
          message: 'Do you want exit?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
                this.alertShown = false;
              }
            },
            {
              text: 'Yes',
              handler: () => {
                console.log('Yes clicked');
                // this.customHandleBackButton();
                this.platForm.exitApp();
              }
            }
          ]
        });
        alert.present().then(() => {
          this.alertShown = true;
        });
      }
    }, 10);
  }
  switchEnviroment(tapEvent) {
    if (tapEvent.tapCount === 5) {
      let profileModal = this.modalCtrl.create(AppInfoPage, { userId: 8675309 });
      profileModal.present();
    }
  }
  getAppVersion() {
    this.appInfo = ''
    try {
      this.appVersion.getAppName().then(appName => {
        this.appName = appName
        this.appInfo += 'app_Name: ' + appName + ',\n'
        this.appVersion.getPackageName().then(packageName => {
          this.appInfo += ('package_Name: ' + packageName + ',\n')
          this.appVersion.getVersionCode().then(buildNo => {
            this.buildNo = buildNo
            this.appInfo += ('version Code: ' + buildNo + ',\n')
            this.appVersion.getVersionNumber().then(versionNumber => {
              this.Version = versionNumber
              this.appInfo += ('versionNumber: ' + versionNumber + ',\n')
              this.getsalons(true)
            })
          })
        })
      })
    } catch (error) {
      this.getsalons(true)
    }
  }
  /**
   * location pop alert to update salon location 
   */
  updateLocationTapped = false;
  updatelocation() {
    this.updateLocationTapped = true;
    this.openLocationSettingIfNeeded()
  }
  openLocationSettingIfNeeded() {
    this.platForm.ready().then(() => {
      if (this.platForm.is('android')) {
        this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {
          this.updateLocationTapped = false;
          isLocationEnabled ? this.getCurrenctLocation() : this.confirmSwitchToSetting()
        }, er => {
          this.updateLocationTapped = false;
        })
        if (this.updateLocationTapped) {
          this.platForm.resume.subscribe(() => {
            this.updateLocationTapped = false;
            this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {
              isLocationEnabled ? this.getCurrenctLocation() : this.confirmSwitchToSetting()
            })
          });
        }
      } else {
        this.getCurrenctLocation()
      }
    })//platform ready
  }
  gotLocationGoigToUpdateCustomeLocation = false
  gpsOptions = { maximumAge: 300000, timeout: 10000, enableHighAccuracy: true };
  latitude: number
  longitude: number
  getCurrenctLocation() {
    if (this.gotLocationGoigToUpdateCustomeLocation) {
      // alert('gonig back cause already got location and pushed to server')
      return
    }
    this.geolocation.getCurrentPosition(this.gpsOptions).then(location => {
      // alert('got location')
      this.latitude = location.coords.latitude
      this.longitude = location.coords.longitude
      this.gotLocationGoigToUpdateCustomeLocation = true;
      this.updateCustomerLocationOnServer()
    }, onError => {
      // alert(JSON.stringify(onError))
      // alert(JSON.stringify(onError.message))
    })
  }
  updateCustomerLocationOnServer() {
    const salID = this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID)
    const params = {
      service: btoa('update_location'),
      sal_id: btoa(salID),
      sal_lat: btoa(this.latitude.toString()),
      sal_lng: btoa(this.longitude.toString()),
    }
    // this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          this.global.makeToastOnSuccess(res.msg)
          this.gotLocationGoigToUpdateCustomeLocation = false
        },
        (error) => {
          // this.global.stopProgress()
          console.log(error);
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        },
        () => {
          this.global.stopProgress()
        }
      )
  }
  confirmSwitchToSetting() {
    if (this.platForm.is('android')) {
      let alert = this.alertCtrl.create({
        title: 'Device location request',
        message: 'Your device location service is not enabled for ' + this.myAppName + '.  Please allow ' + this.myAppName + ' to access your location.',
        buttons: [
          {
            text: 'Deny',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Allow',
            handler: () => {
              console.log('Yes clicked');
              this.diagnostic.switchToLocationSettings()
            }
          }
        ]
      });
      alert.present().then(() => {
      });
    }
  }
  confirmToUpdateLocation() {
    let alert = this.alertCtrl.create({
      title: 'Update ' + this.myAppName + ' location',
      message: 'Do you want to set ' + this.myAppName + ' location to current location?  You can set it later as well using Update Location from menu.',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.updatelocation()
          }
        }
      ]
    });
    alert.present().then(() => {
    });
  }
}
