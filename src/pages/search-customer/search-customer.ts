import { Component, ViewChild, ElementRef, NgZone, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Navbar, Platform } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager'
import { AddNewCustomerPage } from '../../pages/add-new-customer/add-new-customer'
import { GlobalProvider } from '../../providers/global'

import { Customer, Sal_Techs } from '../../providers/salonInterface'
import { Renderer2 } from '@angular/core';
import { last } from 'rxjs/operator/last';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { config } from '../../providers/config';
import { Keyboard } from '@ionic-native/keyboard';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { SlotsViewPage } from '../../pages/slots-view/slots-view'

@IonicPage()
@Component({
  selector: 'page-search-customer',
  templateUrl: 'search-customer.html',
})
export class SearchCustomerPage {
  // @ViewChild('phoneNumber') phoneNumber;
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild('inputFirstDigit') inputFirstDigit: any;
  public unregisterBackButtonAction: any;
  totalPrice=100
  pushTransitions: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  custPhoneNumber: string
  phonePreviousValue = ''
  isServiceCallStarted = false
  loading: any
  public event: any

  selectedTech: Sal_Techs;
  searchTechUrl = config.custImg;
  items: any
  customersArray = []
  customersObjectsArray = []
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: ServiceManager,
    public loadingCtrl: LoadingController,
    public global: GlobalProvider,
    public elem: ElementRef,
    public nativePageTransitions: NativePageTransitions,
    public renderer: Renderer2, public zone: NgZone,
    public keyboard: Keyboard,
    public platform: Platform,
  ) {

    this.selectedTech = navParams.get('selected_Tech');

    this.custPhoneNumber = ''
    // this.initializeItems();
    this.loading = this.loadingCtrl.create({
    });
  }

  setFocus() {
    setTimeout(() => {
      this.inputFirstDigit.setFocus();
      this.keyboard.show()

    }, 1500);

  }
  ionViewDidLoad() {
    this.setFocus()
    // setTimeout(() => {
    //   this.keyboard.show()
    //   this.phoneNumber.setFocus();
    // }, 1500);

    console.log('ionViewDidLoad SearchCustomerPage');
    this.navBar.backButtonClick = (e: UIEvent) => {
      console.log('hehehe');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      this.nativePageTransitions.slide(options)
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop({
          animate: false,
          animation: 'ios-transition',
          direction: 'back',
          duration: 500,
        })
      }

    }

  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  IonViewWillEnter() {
    this.phonePreviousValue = this.custPhoneNumber

  }
  // @IBACTIONS
  customerSelected(cust: Customer) {
    this.isServiceCallStarted = true
    if (cust.cust_id !== undefined) {
      this.nativePageTransitions.slide(this.pushTransitions)
      this.navCtrl.push(SlotsViewPage, {
        cust_id: cust.cust_id,
        selected_Tech: this.selectedTech,
        appo_TotalTime: 10,
        appo_TotalPrice: this.totalPrice,
        appoServicesString: 'Consultancy',
      })
    }
  }
  searchCustomerOnServer() {
   
    const params = {
      service: btoa('search_customer'),
      cust_phone: btoa(this.custPhoneNumber),
      sal_id: this.global.getLoggedInSalonID()
    }

    console.log('in searchCustomerOnServer');
    const _length = this.custPhoneNumber.length
    if (_length === 6) {

      // this.global.showProgress()
      this.serviceManager.getData(params)
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })
        .subscribe((res) => {
          // this.global.stopProgress()
          console.log('6 response .', res)

          if (res.status === this.global.SUCCESS_STATUS && res.customer !== this.global.NO_MATCH_FOUND) {
            this.customersArray = []
            this.customersObjectsArray = []
            this.customersObjectsArray = res.customer
            this.customersArray = this.customersObjectsArray
            console.log('customers data', this.customersObjectsArray);
            if (this.event) {
              this.getItems(this.event)
            }
            this.setFocus()
            // setTimeout(() => {
            //   this.keyboard.show()
            //   this.phoneNumber.setFocus();
            // }, 1500);
          }
        }, error => {
          // this.global.stopProgress()
          this.setFocus()
          //   setTimeout(() => {
          //     this.keyboard.show()
          //   this.phoneNumber.setFocus();
          // }, 1500);
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
    } else if (_length === 11) {
      this.global.showProgress()
      this.serviceManager.getData(params)
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })
        .subscribe((res) => {
          this.global.stopProgress()
          console.log('printing on 11 cust response.', res)

          if (res.status === this.global.SUCCESS_STATUS && res.customer === this.global.NO_MATCH_FOUND) {
            this.nativePageTransitions.slide(this.pushTransitions)
            this.navCtrl.push(AddNewCustomerPage, {
              cust_phone: this.custPhoneNumber
            })
          } else if (res.customer[0].cust_id) {

            const objCustomer: Customer = res.customer[0]
            this.nativePageTransitions.slide(this.pushTransitions)
            this.navCtrl.push(SlotsViewPage, {
              // cust_id: objCustomer.cust_id
              cust_id: objCustomer.cust_id,
              selected_Tech: this.selectedTech,
              appo_TotalTime: 10,
              appo_TotalPrice: this.totalPrice,
              appoServicesString: 'Consultancy',
            })
            this.setFocus()

            // setTimeout(() => {
            //   this.keyboard.show()
            //   this.phoneNumber.setFocus();
            // }, 1500);
          }
        }, error => {

          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
    }
    // setTimeout(function() {
    //   debugger;
    //   document.getElementById('phoneNum').focus();     
    // },1000);
  }

  getItems(event) {
    console.log('ionChange called');
    this.event = event
    console.log(event);
    console.log('new ', this.custPhoneNumber);
    console.log('old: ', this.phonePreviousValue);

    console.log(this.phonePreviousValue);
    let _length = 0
    if (this.custPhoneNumber.length) {
      _length = this.custPhoneNumber.length
    }
    if (_length === 1 && this.custPhoneNumber !== '0') {
      event.value = ''
      this.global.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)
    }
    if (_length === 2 && this.custPhoneNumber !== '03') {
      event.value = '0'
      this.global.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)
    }

    console.log('keyup called');
    var lastChar = event.value[event.value.length - 1];
    console.log('last char', lastChar);

    // let elementChecker = this.custPhoneNumber;//09999
    // const regexp = new RegExp(/^[0-9]$/);
    // let isvalid = regexp.test(lastChar);
    // if (isvalid == false  ) {
    //   this.custPhoneNumber = elementChecker.slice(0, -1);

    //   console.log('invalid input is here');

    // } else {

    //   console.log('valid input');

    // }

    if (_length === 6 && (this.custPhoneNumber.length > this.phonePreviousValue.length)) {
      console.log('calling on length 6');
      this.keyboard.show()
      this.setFocus()
      // this.phoneNumber.setFocus();
      this.searchCustomerOnServer()
    }
    this.phonePreviousValue = this.custPhoneNumber
    if (_length === 11) {
      console.log('is length == 11', _length);
      let found = false
      if (this.customersArray.length !== 0) {
        this.customersArray.forEach(element => {
          const customer: Customer = element
          if (customer.cust_phone.trim() === this.custPhoneNumber.trim()) {
            console.log('customer.cust_phone === this.custPhoneNumber ' + customer.cust_phone + ' === ' + this.custPhoneNumber + '  ' + customer.cust_phone === this.custPhoneNumber);

            found = true
            console.log('wow, i found');
            this.nativePageTransitions.slide(this.pushTransitions)
            this.navCtrl.push(SlotsViewPage, {
              cust_id: customer.cust_id,
             
              selected_Tech: this.selectedTech,
              appo_TotalTime: 10,
              appo_TotalPrice: this.totalPrice,
              appoServicesString: 'Consultancy',
            })
          }
        });

        if (!found) {
          console.log('calling on length 11');
          this.searchCustomerOnServer()
        }
      } else {
        console.log('calling on cust array zero 0');
        this.searchCustomerOnServer()
      }
    }

    if (this.isServiceCallStarted) {
      this.isServiceCallStarted = false
    }
    if (this.customersObjectsArray && this.customersObjectsArray.length > 0) {
      this.customersArray = []
      this.items = []
      if (this.custPhoneNumber.toString().trim() !== '') {
        this.items = this.customersObjectsArray.filter((customerObj) => {
          if (customerObj.cust_phone.toLowerCase().indexOf(this.custPhoneNumber.toString().toLowerCase()) > -1) {
            this.customersArray.push(customerObj)
            return true;
          }
          return false;
        })
      }
    }
  }
  OnValueEnter(value) {
    //firstname =nameOfNgModel

    console.log('keyup called with value', value);
    var lastChar = value[value.length - 1];
    console.log('last char', lastChar);

    let elementChecker = this.custPhoneNumber;//09999
    const regexp = new RegExp(/^[0-9]$/);
    let isvalid = regexp.test(lastChar);
    if (isvalid == false) {
      this.custPhoneNumber = elementChecker.slice(0, -1);

      console.log('invalid input is here');

    } else {
      console.log('valid input');
    }
  }
  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 10);
  }
  //end custom back button for android
}
