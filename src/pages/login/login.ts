import { Component, ViewChild } from '@angular/core';
import { ServiceManager } from '../../providers/service-manager';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global'
import { TabsPage } from '../../pages/tabs/tabs'
import { Device } from '@ionic-native/device';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { Geolocation } from '@ionic-native/geolocation';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

import { config } from '../../providers/config';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild(Content) content: Content;

  email = ''
  password = ''
  gcmAdminDeviceTocken: any;
  // public salonLongitude
  // public salonLatitude
  public myAppName = ''
  public isiOS = false
  public gpsOptions = { maximumAge: 300000, timeout: 10000, enableHighAccuracy: true };

  constructor(

    public navCtrl: NavController,
    public navParams: NavParams,

    public global: GlobalProvider,
    public serviceManager: ServiceManager,
    public statusBar: StatusBar,
    platform: Platform,
    private device: Device,
    public events: Events,
    public platForm: Platform,
    public geolocation: Geolocation,

  ) {
    this.myAppName = config.AppName
    // this.email = 'salon@nopso.co.uk'
    // this.password = '1234'
    // this.email = 'mytestsalon@gmail.com'
    // this.password = '1234'

    this.email = ''
    this.password = ''
    // this.email = 'sc@mail.com'
    // this.password = '1234'
    if (platform.is('ios')) this.isiOS = true; else this.isiOS = false

    console.log('Device UUID is: ' + this.device.uuid);
    platform.ready().then(() => {

      this.statusBar.styleLightContent();
    });
    // this.initFcm();
  }

  ionViewWillEnter() {

  }
  ionViewDidLoad() {
    this.platForm.ready().then(() => {
      this.geolocation.getCurrentPosition(this.gpsOptions).then(location => {
        // this.salonLatitude = location.coords.latitude
        // this.salonLongitude = location.coords.longitude
      }, onError => {

      })
    })

    console.log('ionViewDidLoad LoginPage');
    const isSalonloggedIn = this.global.getFromLocalStorage(this.global.IS_LOGGED_IN)
    console.log('is logged in', isSalonloggedIn);

    if (isSalonloggedIn !== undefined && isSalonloggedIn === '1') {
      console.log('already logged in');
      this.navCtrl.setRoot(TabsPage)

    } else {
      console.log('user is preference does not saved before');

    }
  }
  ionViewDidEnter() {

  }
  permissionRequestCount = 0

  loginUser(form: NgForm) {

    if (this.email.trim() === '' || this.password.trim() === '') {
      this.global.makeToastOnFailure(AppMessages.msgLogin)
      return;
    }

    if (/^\d*$/.test(this.email)) {
      if (this.email.length == 0) {

        this.global.makeToastOnFailure(AppMessages.msgPhoneNumberRequiredForRegistration)
        return
      }
      if (this.email.length < 11 || this.email.length > 11) {

        this.global.makeToastOnFailure(AppMessages.msgCustomerMobileNumber)
        return
      }
    } else if (this.email.trim().length === 0 || !this.validateEmail(this.email)) {
      this.global.makeToastOnFailure(AppMessages.msgLogin)
      return
    }

    let tempToken = this.global.getFromLocalStorage(this.global.GCM_TOKEN);

    // if (!tempToken) {

    //   tempToken = 'cQMM5mq7LDQ:APA91bG_t16tOXi7jSxOWiBzOy_Ea_y47gbhPb_roKQ8Z7wXtNGqFt1t8NKmwX6NdV-lBduduLMxhnoapAaibx0H7CwwjdPXeUaUz5HOAs2UGkPcG9lKItk7nPt2Q_o8RFVIdrFGuKtf'
    // }

    const params = {

      service: btoa('login'),
      sal_email: btoa(this.email),
      sal_password: btoa(this.password),
      admin_device_id: btoa(tempToken),
      admin_device_type: btoa('2'),
      api_key: btoa('abcd1234'),
      // sal_lat: btoa(this.salonLatitude),
      // sal_lng: btoa(this.salonLongitude),
    }

    console.log('login Params', params);
    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log(res);
          const st = res.status
          const er = res.error
          console.log('st:', st, 'error: ', er);
          this.global.stopProgress()
          if (res['status'] === '1' || res['error'] === undefined) {
            // this.global.makeToastOnSuccess('Your are logged in.', 'top')
            console.log(res);
            if (res.sal_id !== undefined) {
              if (res.sal_status == '1' || res.sal_status == 1) {
                this.global.setInLocalStorage(this.global.IS_LOGGED_IN, true)
                this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_ID, res.sal_id)
                this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_NAME, res.sal_name)
                this.global.setInLocalStorage(this.global.LOGED_IN_SALON_PROFILE_IMG, res.sal_pic)
                this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_STATUS, res.sal_status)
                this.global.setInLocalStorage(this.global.LOGGED_IN_SALON_ADDRESS, res.sal_address)
                this.global.setInLocalStorage('zahidz', res.sal_pic)
                this.navCtrl.setRoot(TabsPage)
              } else {
                this.global.makeToastOnFailure(AppMessages.msgSalonRegistration)
              }

            }
          } else {

            this.global.makeToastOnFailure(res.error)
            this.content.scrollTo(0, 1).then(res => {
              this.content.resize()
              this.content.scrollTo(0, 0, 0).then(res => {
              })
            })
          }
          console.log('logged in salon resp');
        }, (error) => {
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          this.global.stopProgress()
          console.log(error);

          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        },
        () => {

          this.global.stopProgress()
        }
      )
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
