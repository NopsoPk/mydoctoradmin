import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {  LoadingController } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager';
import { SharedataProvider } from '../../providers/sharedata/sharedata';
/**
 * Generated class for the PrivacyPolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-privacy-policy',
  templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {
  @ViewChild(Navbar) navBar: Navbar;
  public privacy_policy_test=''
  constructor(
    private serviceManager: ServiceManager,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public nativePageTransitions: NativePageTransitions,
    private sharedProvider: SharedataProvider,
  ) {
  }
  ionViewDidLoad() {
    // this.getPrivacyPolicy()
     
    this.privacy_policy_test = this.sharedProvider.privacy_policy 
    console.log('ionViewDidLoad PrivacyPolicyPage');
    this.navBar.backButtonClick = (e: UIEvent) => {
      console.log('hehehe');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      this.nativePageTransitions.slide(options)
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop({
          animate: false,
          animation: 'ios-transition',
          direction: 'back',
          duration : 500,
        })
      }
      
    }
  }

  getPrivacyPolicy() {
    let loading = this.loadingCtrl.create({ content: "Please Wait..." });
    loading.present();
    var params = {
      service: btoa('privacy_policy'),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        loading.dismiss()
        // alert('response')
      //  this.saveHomePageCounts(response)
      if(response.privacy_policy){
         this.privacy_policy_test=response.privacy_policy[0].config_value;
      }
      console.log('privacy_policy_test',this.privacy_policy_test)
        // console.log('Response',JSON.stringify(response))
      },
        error => {
          loading.dismissAll();
          // this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        });
  }


}
