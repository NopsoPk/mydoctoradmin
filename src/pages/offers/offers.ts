import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { Category, Services, Sal_Techs, Tech_appointments, Salon, Offers } from '../../providers/salonInterface';
import { GlobalProvider } from '../../providers/global';



@IonicPage()
@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {

  offers: Offers[] = [];
  offer: Offers;
  offerId: string;
  totalCost: number;
  noOffers: string;
  public apppointment: Tech_appointments;
  public unregisterBackButtonAction: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    private platform: Platform,
  ) {

    this.offers = navParams.get('offers');

/**
  
 

    // let offerss: Offers[] = navParams.get('offers');
    this.apppointment = navParams.get('apppointment');

    let myDate = new Date();

    var _dayNumber = myDate.getDay()


    if (_dayNumber === 0) {
      _dayNumber = 7
    }

    offerss.forEach(offer => {
      // console.log(JSON.stringify(offer));
      let offerExpirayDate = new Date(offer.so_expiry)

      let offExInMiliSecond = offerExpirayDate.getTime()
      let currentDateInMilisecond = myDate.getTime()

      let soTime: string = offer.so_time
      let isOffersHoursValid: boolean = false
      if (soTime) {
        isOffersHoursValid = false

        if (soTime.trim().length === 0) {
          isOffersHoursValid = true
        } else {

          let _soTime = offer.so_time.split('&')
          if (_soTime.length === 2) {
            soTime = _soTime[1]
            
            let so_Start_time = this.global.getStringFromDateWithFormat(new Date(), 'yyyy-MM-dd') + ' ' + _soTime[0].trim() + ":00";
            let so_End_time = this.global.getStringFromDateWithFormat(new Date(), 'yyyy-MM-dd') + ' ' + _soTime[1].trim() + ":00";

            let dateObjSoStartTime = new Date(this.global.CalendarDateFormat(so_Start_time))
            let dateObjSoEndTime = new Date(this.global.CalendarDateFormat(so_End_time))

            let OfferStartingHours = dateObjSoStartTime.getTime()
            let OffersEndingHours = dateObjSoEndTime.getTime()

            let _appoStart = new Date(this.global.CalendarDateFormat(this.apppointment.app_start_time))
            let appoStartTime = _appoStart.getTime()      

            isOffersHoursValid = appoStartTime >= OfferStartingHours   &&  OffersEndingHours > appoStartTime

            // console.log(' appoStartTime < OffersEndingHours ' + appoStartTime + ' < ' + OffersEndingHours + ' = ' + (appoStartTime < OffersEndingHours));

          }
        }


      }

      // console.log('currentDateInMilisecond < offExInMiliSecond ' + (currentDateInMilisecond < offExInMiliSecond).toString());
      // console.log('offer.so_days.includes(_dayNumber.toString() '+ offer.so_days.includes(_dayNumber.toString()));
      // console.log('!soTime || isOffersHoursValid ' + !soTime +' || '+ isOffersHoursValid);


      if (offExInMiliSecond >= currentDateInMilisecond && offer.so_days.includes(_dayNumber.toString()) && (!soTime || isOffersHoursValid)) {
        this.offers.push(offer)
      }

    });
    // alert(this.offers.length)
    // this.totalCost = navParams.get('cost');
    // if(this.offers === null){
    //   return;
    // }

    */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OffersPage');
    console.log('this.offers', this.offers);
    console.log('this.offers', this.offerId);
    
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }


  selectoffer(offer) {
    this.offerId = offer.so_id;
    this.offer = offer;
    if (this.offer === null) {
      return;
    } else {
      this.viewCtrl.dismiss(this.offer)
    }

    console.log('this.offers', this.offerId);
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 10);
  }

}
