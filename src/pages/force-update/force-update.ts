import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ForceUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-force-update',
  templateUrl: 'force-update.html',
})
export class ForceUpdatePage {
  public forceUpdateMessage = ''
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.forceUpdateMessage = navParams.get('update_msg')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForceUpdatePage');
  }

}
