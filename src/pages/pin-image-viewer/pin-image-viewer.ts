import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,  ViewController } from 'ionic-angular';

/**
 * Generated class for the PinImageViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pin-image-viewer',
  templateUrl: 'pin-image-viewer.html',
})
export class PinImageViewerPage {
 public pinImage = ''
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public view: ViewController,
  ) {
  }

  ionViewDidLoad() {

    let data = this.navParams.get('data')
    console.log(data);
    
  this.pinImage = data.imageURL
  }
  CloseModel() {
    this.view.dismiss()
    console.log('CloseModel');
    
  }
}
