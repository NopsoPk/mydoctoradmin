import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PinImageViewerPage } from './pin-image-viewer';

@NgModule({
  declarations: [
    PinImageViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(PinImageViewerPage),
  ],
})
export class PinImageViewerPageModule {}
