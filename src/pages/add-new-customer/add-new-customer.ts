import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager'
import { Add_New_Customer } from '../../providers/salonInterface'

import { GlobalProvider } from '../../providers/global'
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { SlotsViewPage } from '../slots-view/slots-view';

@IonicPage()
@Component({
  selector: 'page-add-new-customer',
  templateUrl: 'add-new-customer.html',
})
export class AddNewCustomerPage {
  @ViewChild(Navbar) navBar: Navbar;

  pushTransitions: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  txtCustPostCode: string = ''
  txtCustName: string = ''
  txtCustEmail: string = ''
  txtCustPhoneNumber: string
  newCustomer: Add_New_Customer
  phonePreviousValue = ''
  isValidLocation = false
  errorMessageLocation = ''

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: ServiceManager,
    public nativePageTransitions: NativePageTransitions,
    public global: GlobalProvider) {
    this.txtCustName = ''
    this.txtCustPostCode = ''
    this.txtCustEmail = ''
    this.txtCustPhoneNumber = navParams.get('cust_phone');
    console.log('customer phone delievered successfully');
    console.log(this.txtCustPhoneNumber);
    this.newCustomer = {
      cust_id: null,
      status: null,
      cust_pic_name: null,
      sms_message: null,
      sms_status: null,

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewCustomerPage');

    this.navBar.backButtonClick = (e: UIEvent) => {
      console.log('hehehe');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      this.nativePageTransitions.slide(options)
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop({
          animate: false,
          animation: 'ios-transition',
          direction: 'back',
          duration: 500,
        })
      }

    }
  }

  //    Custome Functions
  onInput() {
    if (this.txtCustPostCode) {
      this.txtCustPostCode = this.txtCustPostCode.toLocaleUpperCase()
    }
  }

  //    @IBACTIONS
  btnNextTapped() {

    const salonID = this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID)
    if (salonID.length === 0) {
      this.global.makeToastOnFailure(AppMessages.msgSalonIdMissing)
      console.log('Salon Id is not found. Please login your salon again.');
      return
    }
    if (this.txtCustName.length === 0) {
      this.global.makeToastOnFailure(AppMessages.msgCustomerName)
      return
    }

    if (this.txtCustPostCode.length > 0 && this.isEmail(this.txtCustEmail.toString()) == false) {
      this.global.makeToastOnFailure(AppMessages.msgInvalidEmail)
      return
    }

    console.log('btn Next Tapped.');

    const params = {
      service: btoa('register'),
      cust_name: btoa(this.txtCustName),
      cust_email: btoa(this.txtCustEmail),
      cust_phone: btoa(this.txtCustPhoneNumber),
      cust_zip: btoa(this.txtCustPostCode),
      sal_id: btoa(salonID),
    }
    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(Response => {

        this.global.stopProgress()
        this.newCustomer.cust_id = Response.cust_id;

        if (this.newCustomer.cust_id === undefined) {
          this.global.makeToastOnFailure(AppMessages.msgCustomerID)
          console.log('could not fetch data from server.')
          return
        } else {

          this.nativePageTransitions.slide(this.pushTransitions)
          this.navCtrl.push(SlotsViewPage, {
            cust_id: Response.cust_id,

            // selected_Tech: selectedTech,
            appo_TotalTime: 10,
            appo_TotalPrice: '0',
            appoServicesString: 'Consultancy',
          })
        }

      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });

  }

  isEmail(search: string): boolean {
    var isValid: boolean;

    const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    isValid = regexp.test(search);
    console.log(isValid)
    return isValid
  }

  OnValueEnter(value) {
    this.txtCustPostCode = this.txtCustPostCode.toUpperCase()
  }

  isPostCodeValid(): boolean {

    if ((this.txtCustPostCode.length < 6)) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Please enter a valid postcode including a space.'
      return this.isValidLocation

    } else if (!this.checkSpaces()) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Invalid postcode'
      return this.isValidLocation
    }
    return true
  }
  checkSpaces() {
    let spaceFound = false;
    let spaceCOunter = 0;
    let text = this.txtCustPostCode;
    for (let i = 0; i < text.length; i++) {
      let valueATIndex = text.charAt(i);
      let value = valueATIndex.toString();
      console.log('value', value);
      if (value == ' ') {
        console.log('spaceFound', value);
        spaceFound = true;
      } else {
        spaceFound = false;
      }
      if (spaceFound) {
        spaceCOunter = (spaceCOunter) + 1;
      }
    }
    console.log('spaceCOunter', spaceCOunter);
    if (spaceCOunter < 1 || spaceCOunter > 1) {
      return false;
    }
    return true;
  }

}
