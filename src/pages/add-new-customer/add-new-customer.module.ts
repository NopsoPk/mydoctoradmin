import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNewCustomerPage } from './add-new-customer';

@NgModule({
  declarations: [
    AddNewCustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNewCustomerPage),
  ],
})
export class AddNewCustomerPageModule {}
