import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermsCondPage } from './terms-cond';

@NgModule({
  declarations: [
    TermsCondPage,
  ],
  imports: [
    IonicPageModule.forChild(TermsCondPage),
  ],
})
export class TermsCondPageModule {}
