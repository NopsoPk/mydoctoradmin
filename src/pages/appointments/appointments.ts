import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { App, IonicPage, NavController, NavParams, LoadingController, Slides } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager';
import { Salon, Sal_Techs, Tech_appointments, Services, Category } from '../../providers/salonInterface';
import { GlobalProvider } from '../../providers/global'

import { config } from '../../providers/config';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { Observable } from "rxjs/Observable";
import { Subscription } from 'rxjs/Subscription';
import { SharedataProvider } from '../../providers/sharedata/sharedata';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { SlotsViewPage } from '../slots-view/slots-view';

@IonicPage()
@Component({
  selector: 'page-appointments',
  templateUrl: 'appointments.html',
})
export class AppointmentsPage implements OnInit {
  @ViewChild(Slides) slides: Slides;

  heightOfCalendar = 100;
  breakTimeY = 0;
  breakTimeHeight = 0;
  times: string[] = [];
  public scrollAmount = 0;
  dateSelected: Date = new Date();
  pixelsPerMinute = 4
  showAppTop = false;
  showDate: Date;
  private params: any = null;
  salonSubscription: Subscription;
  public calendarOption;
  selectedDateStr: any
  sortedSlots = []
  selectedDateIndex = 0
  dateComponent = []
  staticCalendar = []
  dateString: string
  appStartTime: string
  monthNames = [
    'January', 'February', 'March',
    'April', 'May', 'June',
    'July', 'August', 'September',
    'October', 'November', 'December'
  ];
  isToday: boolean;

  calendarOneWeek: Date;

  salonServicesCategories: Salon;
  salonTechs: Sal_Techs[] = [];
  salonTechsToShow: Sal_Techs[] = [];
  selectedTech: Sal_Techs = null;
  appointments: Tech_appointments[] = [];
  appointment: any;
  techServices: Services[];
  serviceCategories: Category;
  offers: any;
  salonDetails = []
  selectedTechIndex = 0;

  techimgUrl: string;
  custimgUrl: string;

  Serving: Boolean = true;
  calenderview: Boolean = false;
  activetech: Boolean = false;
  salonData: any
  shouldShowCalender = true

  constructor(public navCtrl: NavController,
    public navParams: NavParams, public global: GlobalProvider,
    public loadingCtrl: LoadingController,
    private serviceManager: ServiceManager,
    public appCtrl: App,
    public zone: NgZone,
    private sharedProvider: SharedataProvider,
  ) {

    console.log(this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS));
    let future = this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS);
    console.log('Future...' + future);
    this.calendarOption = {
      fontSize: 4.23,
      fontWeight: 600,
      numberOfDays: future,
      selectedColor: 'black',
      defaultColor: 'white',
      circleBorderColor: '1px solid #969696',
      selectedDateColor: 'white',
      seletedDayColor: 'white',
      selectedMonthAndYearColor: 'rgba(255,255,255,0.5)',
      unSelectedDateColor: 'rgb(150,150,150)',
      circleBackgroundColor: 'transparent',
      hieghtOfCalendar: '140px',
      calendarBackgroundColor: 'transparent',
      sizeOfCircle: 6.5,
      };

  }

  ngOnInit() {
    this.stopSubscription();
  }

  // scrollHandler(event) {
  // console.log('Direction', event.scrollTop);

  // let scrollposition = Math.round(event.scrollTop);
  // if(scrollposition > 250 && this.appointments.length > 3) {
  //   this.showAppTop = true;
  //   const date = new Date();
  //   const headerDate = this.dateSelected ? this.dateSelected : date;
  //   this.showDate = headerDate;
  //   // console.log('this.selectedTech', this.selectedTech);
  // } else {
  //   this.showAppTop = false;
  // }

  // console.log(`ScrollEvent: ${event}`)
  // this.zone.run(()=>{
  // since scrollAmount is data-binded,
  // the update needs to happen in zone
  //     this.scrollAmount++;
  //   })
  // }

  ionViewWillLeave() {
    this.stopSubscription();
    this.global.setInLocalStorage(this.global.APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED, '')
  }

  ionViewDidLoad() {
    this.techimgUrl = config.techImg;
    this.custimgUrl = config.custImg;
    const customeDate = this.global.getCurrentDeviceDate('')
    console.log('custome date', customeDate);
    this.dateComponent = customeDate.split('-')
    console.log('day should be', this.dateComponent[2]);
    let currentDate = Number(this.dateComponent[2])

    for (let index = 0; index < 7; index++) {
      this.staticCalendar.push(currentDate)
      if (currentDate === 31) {
        currentDate = 0
      }
      currentDate++
    }
    console.log('ionViewDidLoad DashboardPage');
  }
  ionViewCanEnter() {
  }

  ionViewWillEnter() {
    this.salonTechsToShow = []
    this.global.setInLocalStorage(this.global.APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED, '')
    this.stopSubscription();
    this.sharedProvider.makeAppointmentSource.subscribe((res) => {
      if (res) {
        if(!this.salonTechsToShow || this.salonTechsToShow.length === 0){
          this.getsalonData(this.dateSelected,true)   
        }else{
          this.getsalonData(this.dateSelected, false)
          this.getappointments(this.selectedTech, this.selectedTechIndex)
        }
      }
    });
    // this.sharedProvider.techIndex.subscribe((res) => {
    //   console.log('appointment completed of tech index', res);
    //   if (res) {
    //     this.selectedTechIndex = res;
    //      console.log('appointment completed of tech index', res);
    //     //  this.getAppointments(res, res.tech_id );
    //      this.getAppointments(this.sal_techs[res], res);         
    //   }
    // });

    this.getsalonData(this.dateSelected, false)

  }

  ionViewDidEnter() {
    this.stopSubscription();
    this.shouldShowCalender = true

    this.dateSelected ? this.getsalonData(this.dateSelected, true) : this.getsalonData(this.global.getCurrentDeviceDate(''), true)

  }

  createTimesArray() {
    console.log("Times mehtod...");
    let salHours = this.salonData['sal_hours'].split(',');
    console.log('salhours', salHours);
    let todaysHours;
    if (this.dateSelected.getDay() != 0) {
      todaysHours = salHours[this.dateSelected.getDay() - 1];
      let splittedHours = todaysHours.split('&');
      let startHour = splittedHours[0].split(':')[0];
      let startMinute = splittedHours[0].split(':')[1];

      let endHour = splittedHours[1].split(':')[0];
      let endMinute = splittedHours[1].split(':')[1];

      this.createSlots(startHour, startMinute, endHour, endMinute);
    } else {

    }

  }
public salonIsOff = false;
  createSlots(startHour, startMinute, endHour, endMinute) {
    console.log('hour' + startHour + 'Minute' + startMinute + 'hour' + endHour + 'minute' + endMinute)
    if (startHour > endHour) {

      this.global.makeToastOnFailure(AppMessages.msgSalonTimeConfiguration);
      this.shouldShowCalender = false
      return;
    } else if (startHour == endHour) {
      this.salonIsOff = true
      // this.global.makeToastOnFailure(AppMessages.msgOffSalon);
      return;
    } else {
      this.salonIsOff = false
      let slot = 1;
      this.heightOfCalendar = (endHour - startHour) * 60 * 4;
      this.times = [];
      for (let i = startHour; i < endHour; i++) {
        for (let j = 0; j < 60; j++) {
          if (j == 0) {
            this.times.push((('0' + i).slice(-2)) + ":00");
          }
          else {
            this.times.push((('0' + i).slice(-2)) + ":" + (('0' + j * slot).slice(-2)));
          }
        }
      }
      // this.times.push((endHour) + ":00");

      if (endMinute > 0) {
        for (let k = 0; k < endMinute; k++) {
          this.times.push(endHour + ":" + (('0' + k * slot).slice(-2)))
        }
      }
      if (startMinute > 0) {
        for (let k = 0; k < startMinute; k++) {
          this.times.shift();
        }
      }

    }

    this.calculateBreakTime();

  }

  resetBreaktime() {
    this.breakTimeHeight = 0;
    this.breakTimeY = 0;
  }

  calculateBreakTime() {
    if (this.salonTechs.length == 0 || this.salonTechs == undefined) {
      return;
    }
    let tempTech = this.selectedTech;
    console.log('calculateBreakTime , tempTech => ', tempTech);

    let breakTime = []

    if (tempTech['tech_break_time']) {
      breakTime = tempTech['tech_break_time'].split(',')
    } else {
      console.log('Break time => ', breakTime)
      this.resetBreaktime()
      return
    }
    console.log('calculateBreakTime , Break time => ', breakTime)
    console.log('selected date => ', this.dateSelected)

    let dayOfTheWeekk = this.dateSelected.getDay()
    console.log('day of the week =>', dayOfTheWeekk)
    --dayOfTheWeekk
    if (dayOfTheWeekk == -1) {
      dayOfTheWeekk = 6
    }// else {
    //   dayOfTheWeekk -= 1
    // }

    console.log('breakTime of the day =>', breakTime[dayOfTheWeekk])
    if (breakTime[dayOfTheWeekk] === undefined) {
      return;
    }
    let breakStart = breakTime[dayOfTheWeekk].split('&')[0]
    let breakEnd = breakTime[dayOfTheWeekk].split('&')[1]

    if (breakStart === undefined) {
      return;
    }
    if (breakEnd === undefined) {
      return;
    }

    if (breakStart != breakEnd) {
      let breakStartIndex = this.times.indexOf(breakStart)
      let breakEndIndex = this.times.indexOf(breakEnd)

      if (breakStartIndex != -1 && breakEndIndex != -1) {
        this.breakTimeHeight = (breakEndIndex - breakStartIndex) * this.pixelsPerMinute
        this.breakTimeY = breakStartIndex * this.pixelsPerMinute;
      }
    } else {
      this.resetBreaktime()

    }

    // let breakTime =[]
    // if (tempTech['tech_break_slots'] !== undefined) { breakTime = tempTech['tech_break_slots'].split('&'); }
    // console.log(tempTech);
    // this.breakTimeY = this.getTimeFromSlot(breakTime[0]) * 4;
    // this.breakTimeHeight = (breakTime[1] - breakTime[0]) * 20;
  }

  getTimeFromSlot(slot) {
    return this.times.indexOf(this.times[(slot - 1) * 5]);
  }

  getIndexFromTime(time) {
    return this.times.indexOf(time);
  }

  getAppoHeight(appo) {
    return appo.app_est_duration * 4;
  }

  getMarginTop(appo) {
    return -(this.heightOfCalendar - this.getIndexFromTime(appo.app_start_time.split(' ')[1].substr(0, 5)) * 4);
  }

  // gotoSlide(tech: Sal_Techs, index) {
  //   let currentIndex = this.slides.getActiveIndex();
  //   console.log('Current index is', currentIndex);

  //   this.slides.slideTo(index, 500);
  // }

  getappointments(tech: Sal_Techs, index) {
    this.selectedTechIndex = index;
    this.activetech = true;
    this.selectedTech = this.salonTechsToShow[this.selectedTechIndex]
   
    // this.selectedTech = tech;
    this.createTimesArray()
    // this.appointments = tech.tech_appointments;

    if (this.selectedTech.tech_appointments && this.selectedTech.tech_appointments.length > 0) {
      //    let appointments = [...tech.tech_appointments];
      this.appointments = this.selectedTech.tech_appointments.filter(function (appo) {
        // console.log('appo .... status');

        return appo.app_status !== 'rejected' && appo.app_status !== 'deleted'
      })
      console.log('getappointments , this.appointments =>> ', this.appointments)

    } else {

      this.appointments = []
    }

    this.techServices = this.selectedTech.tech_services;
    console.log(this.appointments);
  }

  calenderswitch() {
    console.log('calendaer value', this.calenderview);

    if (this.shouldShowCalender === true) {
      this.calenderview = !this.calenderview;
    } else {
      this.global.makeToastOnFailure(AppMessages.msgSalonTimeConfiguration);
    }
    console.log('calendaer value', this.calenderview);
  }

  callToCust(cust_phone: string) {

    window.open('tel:' + cust_phone);

  }

  toggleDetails(data) {

    if (data.showDetails) {
      data.showDetails = false;
      data.icon = 'add';
    } else {
      data.showDetails = true;
      data.icon = 'remove';
    }
  }

  refreshAppointment(appointment) {

    if (this.dateSelected) {
      this.getsalonData(this.dateSelected, true)
    } else {
      this.getsalonData(this.global.getCurrentDeviceDate(''), true)
    }

  }

  private stopSubscription() {
    if (this.salonSubscription) {
      this.salonSubscription.unsubscribe();
    }
  }

  ngOnDestroy() {
    this.stopSubscription();
  }

  private subscribeToData() {
    let _AppoDate = ''
    if (this.dateSelected) {
      _AppoDate = this.dateString
    }
    // this.salonSubscription = Observable.timer(40000).first().subscribe(() => this.getsalonData(this.global.getCurrentDeviceDate(''), false));
    this.salonSubscription = Observable.timer(40000).first().subscribe(() => this.getsalonData(this.dateSelected, false));
  }

  isWentToCallService = false
  getsalonData(date: any, isShow) {
    if (this.isWentToCallService)
    {
      return 
    }else{
      this.sharedProvider.showCalenderView = false
    }
    
    let _sal_last_fetched_data = this.global.getFromLocalStorage(this.global.APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED)
    if (!_sal_last_fetched_data) _sal_last_fetched_data = ''

    this.params = {
      service: btoa('sal_servs_techs'),
      platform: btoa('ios'),
      app_version: btoa('1.1.6'),
      app_name: btoa('SalonAppPhoneAdmin'),
      sal_id: btoa(this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID)),
      app_date: null,
      tech_services: btoa('1'),
      last_fetched: btoa(_sal_last_fetched_data),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
      app_status: btoa(this.global.APP_STATUS_ACCEPTED + ',' + this.global.APP_STATUS_FINISHED + ',' +
        this.global.APP_STATUS_PENDING + ',' + this.global.APP_STATUS_SERVING
      )
    }
    this.params.app_date = btoa(this.global.getCurrentDeviceDate(date.toString()));
    this.params.appointmentDate = btoa(this.global.getCurrentDeviceDate(date.toString()));
    if (isShow) {
      this.global.showProgress()
    }
    let $comp = this;
    this.isWentToCallService = true
    this.serviceManager.getData(this.params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        // this.global.setInLocalStorage('resresresres',res)
        this.sharedProvider.showCalenderView = true
        this.isWentToCallService = false
        if (res) {
          this.global.stopProgress()
          this.global.setInLocalStorage(this.global.APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED, res.response_datetime)
          if (res && Number(res.is_modified) !== 1) {
            return
          }

          console.log('getsalonData => ', res.salon);
          this.salonData = res.salon;
          this.serviceCategories = res.salon.service_categories;

          this.salonTechsToShow = res.salon.sal_techs;
          if (this.salonTechs.length === 0) {
            this.salonTechs = res.salon.sal_techs;
          }

          // 

          this.subscribeToData();
          if (this.salonTechs === null || this.salonTechs === undefined || this.salonTechs.length == 0) {
            this.global.makeToastOnFailure(AppMessages.msgTechnicianMissing)
            return
          }
          if (!this.selectedTech) {
            this.selectedTech = this.salonTechsToShow[0]
            this.selectedTechIndex = 0;
          } else {
            let temp = this.selectedTech

            // this.selectedTech = this.salonTechs.find(function (item) { return item.tech_id === temp.tech_id })

            this.salonTechsToShow.forEach((Technician, index) => {
              // let selectedTech: Sal_Techs = Technician;
              if (Technician.tech_id === temp.tech_id) {
                this.selectedTech = Technician
                this.selectedTechIndex = index
              }

            });

          }

          console.log(JSON.stringify(this.selectedTech));

          this.getappointments(this.selectedTech, this.selectedTechIndex)

          $comp.createTimesArray();
          if (this.salonTechs === null) {
            return;
          }
          this.techServices = res.salon.sal_techs.tech_services;

          // this.getsalonData();
          // localStorage.setItem('salon', JSON.stringify(res.salon));

          // this.sharedProvider.showCalenderView = true

        }
        this.global.stopProgress()
      },
        error => {
          this.sharedProvider.showCalenderView = true
          this.isWentToCallService = false
          this.global.stopProgress()
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  rescheduleAppointment(appointment: Tech_appointments) {
    console.log(appointment);
    // const devicedate: string = new Date().toISOString();
    const _salonData = this.global.getFromLocalStorage(this.global.LOGED_IN_SALON_DATA)
    this.appCtrl.getRootNav().push(SlotsViewPage, {
      appointment,
      isRescheduleAppointment: true,
      techIndex: this.selectedTechIndex,
      selected_Tech: this.selectedTech,
      selected_TechIndex: this.selectedTechIndex,
      appo_TotalTime: 10,
      appo_TotalPrice: appointment.app_price,
      appoServicesString: appointment.app_services,
      cust_id: appointment.cust_id,
      isReschedule: true,
    })
  }

  removeAppointment(appointment: Tech_appointments) {
    // console.log(appointment);
    // this.global.loading.present()
    // const devicedate: string = new Date().toISOString();
    const paramsToDelete = {
      service: btoa('cancel_app'),
      app_id: btoa(appointment.app_id),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
    }
    this.global.showProgress();

    this.serviceManager.getData(paramsToDelete)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        this.global.stopProgress();
        // console.log(res);
        // this.getsalons();
      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')

          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);

        });
  }

  dateDidSelect(date, index) {
    this.sortedSlots = []
    const d = new Date();
    if (Number(date.toString()) < 9) {
      date = '0' + date.toString();
    }
    console.log('The current month is ' + this.monthNames[d.getMonth()]);
    this.selectedDateStr = date + ' ' + this.monthNames[d.getMonth()]
    this.selectedDateIndex = index
    this.dateComponent[2] = date
    this.dateString = this.dateComponent[0] + '-' + this.dateComponent[1] + '-' + this.dateComponent[2]
    const dateParsing = Date.parse(this.dateString)
    if (!dateParsing) { return }
    this.sortedSlots = []
    console.log('selected Date: ', this.dateString);

    this.getsalonData(this.dateString, true)
  }
  getFromattedTime(appointmentTime) {
    return this.global.getFormattedTime(appointmentTime)// this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
  }

  dateClicked(date) {

    this.global.setInLocalStorage(this.global.APPOINTMENTS_LAST_FETCHED_DATE_FOR_ISMODIFIED, '')
    this.dateSelected = date;
    this.getsalonData(date, true)
    console.log(date);

  }

  get12HourFormatFrom24Hour(time) {
    let hours = time.split(':')[0];
    var suffix = Number(hours) >= 12 ? "PM" : "AM";
    var hour = ((Number(hours) + 11) % 12 + 1);
    if (hour < 10) {
      return "0" + hour + ':' + time.split(':')[1] + ' ' + suffix;
    }

    return hours = hour + ':' + time.split(':')[1] + ' ' + suffix;
  }
}
