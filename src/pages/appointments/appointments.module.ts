import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsPage } from './appointments';
import { PrevAppComponent } from '../../components/prev-app/prev-app';
import { AppointmentsPageDetailComponent } from '../../components/appointments-page-detail/appointments-page-detail';


@NgModule({
  declarations: [
    AppointmentsPage, PrevAppComponent,AppointmentsPageDetailComponent,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentsPage),
  ],
})
export class AppointmentsPageModule {}
