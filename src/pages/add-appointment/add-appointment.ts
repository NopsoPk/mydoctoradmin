import { Component, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Slides, Navbar, Platform } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager'
import { Services, Sal_Techs, Tech_appointments, Salon, Category } from '../../providers/salonInterface'
import { GlobalProvider } from '../../providers/global'
import { SlotsViewPage } from '../../pages/slots-view/slots-view'
import { config } from '../../providers/config';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SharedataProvider } from '../../providers/sharedata/sharedata';
import { first } from 'rxjs/operators';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

@Component({
  selector: 'page-add-appointment',
  templateUrl: 'add-appointment.html',
})
export class AddAppointmentPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Navbar) navBar: Navbar;

  pushTransitions: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public salonData: Salon

  baseUrl = config.techImg;

  public scrollAmount = 0;
  shouldGetTechServices = true
  cust_id: string;
  techData: Sal_Techs[];
  
  salonDetails = []
  // _services: any[]
  selectedTechIndex: number;
  techimgUrl: string;
  custimgUrl: string;

  contentheight: number;

  salonServices: Services
  techServies: any;
  servicesSortedByCat = []
  Categories = []
  selectedServicesObjects = []
  public salonServicesCategories: Category[]
  techIndex: number;

  totalPrice = 0.0
  totalTime = 0
  servicesCount = [];
  selectedTech: Sal_Techs;
  selectedTechId: number;
  isAppoTechChanged = false
  public unregisterBackButtonAction: any;

  // # Nav Params
  isRescheduleAppointment = false
  appServices = [];
  appointment: Tech_appointments;

  // #Params to send
  techID: any
  public currencySymbol = ''

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public serviceManager: ServiceManager,
    public global: GlobalProvider,
    public zone: NgZone,
    private sharedProvider: SharedataProvider,
    public nativePageTransitions: NativePageTransitions,
    private platForm: Platform,
  ) {
    this.salonServicesCategories = []
    this.isRescheduleAppointment = navParams.get('isRescheduleAppointment');
    this.cust_id = navParams.get('cust_id');
    console.log('cust id is : ', this.cust_id);

    if (this.isRescheduleAppointment) {

      this.appointment = navParams.get('appointment');

      this.selectedTechId = this.appointment.tech_id;
      this.techIndex = navParams.get('techIndex');
      console.log('tech id is: ', this.selectedTechId);

      // array.forEach(element => {

      // });
    } else {
      this.techIndex = 0;
    }
    // this.contentheight = this.servicesSortedByCat.length * this.Categories.length + 12;
  }
  scrollHandler(event) {

    this.zone.run(() => {
      // since scrollAmount is data-binded,
      // the update needs to happen in zone
      this.scrollAmount++
    })
  }

  ionViewWillEnter() {
    this.currencySymbol = this.sharedProvider.currencySymbol

    this.slides.slideTo(this.techIndex, 500);
    if (this.selectedServicesObjects !== undefined) {
      console.log('selected services objects', this.selectedServicesObjects);

    }
    console.log('will enter.');
  }
  ionViewDidLoad() {
    this.techimgUrl = config.techImg;
    this.custimgUrl = config.custImg;
    // this.getsalons()
    this.loadTechData()

    this.salonData = this.global.getFromLocalStorage(this.global.LOGED_IN_SALON_DATA);
    console.log(JSON.stringify(this.salonData));

    if (this.salonData) {
      this.salonServicesCategories = this.salonData.service_categories

      let salonServices: Services[] = this.salonData.services

      if (this.isRescheduleAppointment) {
        if (this.salonServicesCategories) {
          this.selectedServicesObjects = []
          let appoSevices = this.appointment.app_services.split(',')

          this.salonServicesCategories.forEach(serviceCategory => {
            let services: Services[] = []

            salonServices.forEach(salService => {
              if (serviceCategory.ssc_id === salService.ssc_id) {
                services.push(salService)
                salService.checked = false
                appoSevices.forEach(appoService => {
                  if (appoService.toLowerCase() === salService.sser_name.toLowerCase()) {

                    this.calculateAppoCostPrice(salService)

                    salService.checked = true
                  }

                });

              }

              return salService
            });
            return serviceCategory.services = services
          });
        }
      } else {

        if (this.salonServicesCategories) {

          this.salonServicesCategories.forEach(serviceCategory => {
            let services: Services[] = []

            salonServices.forEach(salService => {
              if (serviceCategory.ssc_id === salService.ssc_id) {
                services.push(salService)
              }
              return salService.checked = false
            });
            return serviceCategory.services = services
          });
        }
      }

    }
    console.log('ionViewDidLoad');
    // this.showAppoServicesSelected()

    this.navBar.backButtonClick = (e: UIEvent) => {
      console.log('hehehe');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      this.nativePageTransitions.slide(options)
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop({
          animate: false,
          animation: 'ios-transition',
          direction: 'back',
          duration: 500,
        })
      }

    }

  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  // @IBACTIONS

  gotoSlide(index) {
    // let currentIndex = this.slides.getActiveIndex();
    // console.log('Current index is', currentIndex);
    this.selectedTechIndex = index;
    this.slides.slideTo(index, 500);
  }

  selectedTechDetailTapped(selectedTech, app_tech_id, index) {
    console.log('indx is: ', index);
    // this.selectedTechIndex = index;

    const yourImg = document.getElementById('imgSelectedTech');
    if (yourImg && yourImg.style) {
      yourImg.style.height = '100px';
      yourImg.style.width = '200px';
    }
    console.log('came in selected tech func', selectedTech);
    this.selectedTech = selectedTech
    this.selectedTechId = selectedTech.tech_id

    console.log('tech id is: ', this.selectedTechId);

    // Step:1 Getting Techh ID
    this.techID = app_tech_id ? app_tech_id : selectedTech.tech_id;
    let techServices = []
    const salonTechs = this.global.getFromLocalStorage(this.global.SALON_TECHS)
    if (salonTechs === undefined) {
      console.log('salon techs found nil');
      return;
    }
    // Step:2 Matching Techh ID and Getting Services
    salonTechs.forEach(tech => {
      if (tech.tech_id === this.techID) {
        console.log('tech id matched');

        techServices = tech.tech_services
        this.selectedTech = tech
      }
    });
    /**
     * 
     * 
     */
    // Step:3 Removing Duplicate Categories to get Unique Categories
    this.Categories = this.removeDuplicates(techServices, 'ssc_name')

    // Step:4 Sorting Tech services BY Category Name
    this.servicesSortedByCat = []
    this.Categories.forEach(category => {
      const catFromCategory: string = category['ssc_name'];
      techServices.forEach(element => {
        const catFromService: string = element['ssc_name'];
        if (catFromCategory.trim() === catFromService.trim()) {
          const elementNew = element;
          this.servicesSortedByCat.push(elementNew)
        }
      });
    });

    console.log('servicesSortedByCat', this.servicesSortedByCat);

    // this.totalPrice = 0.0
    // this.totalTime = 0
    // this.selectedServicesObjects = []
    // this.isAppoTechChanged = true
    // this.showAppoServicesSelected()
  }
  btnNextTappe() {
 
    console.log('btn Next Tapped');

    if (this.selectedTech === undefined) {
      this.global.makeToastOnFailure(AppMessages.msgTechnicianSelection)
      console.log('please select your technician');
      return
    }
    //we are not selecting any service
    //commented by rizwan
    // if (this.selectedServicesObjects.length === 0) {
    //   this.global.makeToastOnFailure(AppMessages.msgSelectService)
    //   console.log('no service selected');
    //   return
    // }

    console.log('btnNext Tapped');
    console.log('selected tech is: ', this.selectedTech);
 //commented by rizwan
    // let appoServices = ''
    // this.selectedServicesObjects.forEach(element => {
    //   const service: Services = element
    //   console.log('length', this.selectedServicesObjects.length);
    //   console.log('service obj', service);

    //   appoServices += service.sser_name + ','
    // });
    // if (this.isRescheduleAppointment) {
    //   this.cust_id = this.appointment.cust_id
    // }

    // console.log('appo services ', appoServices);
    // appoServices = appoServices.slice(0, -1)
    // console.log('appo services after removing last character', appoServices);
    // console.log('selected tech');
    // console.log(this.selectedTech);
    // console.log('total time', this.totalTime);
    // console.log('total price', this.totalPrice);
    // console.log('slected services obj', JSON.stringify(this.selectedServicesObjects));
    // console.log('appo service string', appoServices);
    // console.log('cust id', this.cust_id);
    // console.log('is reschedule ', this.isRescheduleAppointment);
    // console.log('appointment obj', this.appointment);

    if (this.isRescheduleAppointment) {
      console.log('is reschedule ', this.isRescheduleAppointment);
      this.nativePageTransitions.slide(this.pushTransitions)
      this.navCtrl.push(SlotsViewPage, {
        // selected_Tech: this.selectedTech,
        // selected_TechIndex: this.selectedTechIndex,
        // appo_TotalTime: this.totalTime,
        // appo_TotalPrice: this.totalPrice,
        // appo_Services: this.selectedServicesObjects,
        // appoServicesString: appoServices,
        // cust_id: this.cust_id,
        // isReschedule: this.isRescheduleAppointment,
        // appointment: this.appointment,
        // isRescheduleAppointment: true,
        // techIndex: this.selectedTechIndex,

        appointment: this.appointment,
        isRescheduleAppointment: true,
        techIndex: this.selectedTechIndex,
        selected_Tech: this.selectedTech,
        selected_TechIndex: this.selectedTechIndex,
        appo_TotalTime: 10,
        appo_TotalPrice: this.totalPrice,
        appoServicesString: this.appointment.app_services,
        cust_id:  this.cust_id,
        isReschedule: true,
      })
    } 
    //other scanario is comminted for now just reschedule is working
    // else {
    //   console.log('making new appointment ', this.isRescheduleAppointment);
    //   this.nativePageTransitions.slide(this.pushTransitions)
    //   console.log('selected_Tech', JSON.stringify(this.selectedTech))
    //   console.log('selected_TechIndex',this.selectedTechIndex)
    //   console.log('appo_TotalTime',this.totalTime)
    //   console.log('appo_TotalPrice',this.totalPrice)
    //   console.log('selectedServicesObjects',JSON.stringify(this.selectedServicesObjects))
    //   console.log('appoServices',appoServices)
    //   this.navCtrl.push(SlotsViewPage, {
    //     selected_Tech: this.selectedTech,
    //     selected_TechIndex: this.selectedTechIndex,
    //     appo_TotalTime: this.totalTime,
    //     appo_TotalPrice: this.totalPrice,
    //     appo_Services: this.selectedServicesObjects,
    //     appoServicesString: appoServices,
    //     cust_id: this.cust_id,
    //   })
    // }
  }
  // CUSTOME FUNCTIONS
  showAppoServicesSelected() {

    if (!this.isRescheduleAppointment) {
      return
    }
    // Step:1 Taking out Appointment Services to show them SELECTED.
    console.log('appointment obj', this.appointment);
    if (this.appointment === undefined) {
      return
    }
    this.appServices = this.appointment.app_services.split(',');
    console.log(this.appServices);

    // Step:1 Show tech selected.
    const $comp = this;
    // if (!this.isAppoTechChanged) {
    //   this.selectedTechDetailTapped('', this.appointment.tech_id, '');
    // }

    this.Categories.forEach(function (item) {
      const tempservices = $comp.servicesSortedByCat.filter(function (serv) {
        return serv.ssc_name === item.ssc_name
      })
      item.services = tempservices
    })

    this.Categories.forEach(category => {
      const data = category;
      data.services.forEach(service => {
        service.checked = false;
      });
    });
    console.log('Unique category Objects', this.Categories)
    this.selectedServicesObjects = []
    this.totalPrice = 0.0
    this.totalTime = 0.0

    for (let j = 0; j < this.appServices.length; j++) {

      this.checked(this.appServices[j])
    }

  }
  checked(serviceName) {
    for (let catInex = 0; catInex < this.Categories.length; catInex++) {
      for (let k = 0; k < this.Categories[catInex].services.length; k++) {

        // console.log(this.Categories[catInex].services[k].sser_name)
        if (serviceName.trim() === this.Categories[catInex].services[k].sser_name.trim()) {
          const serviceObject = this.Categories[catInex].services[k]
          this.calculateAppoCostPrice(serviceObject)
          this.Categories[catInex].services[k].checked = true
          console.log('services obj count ', this.selectedServicesObjects.length);

        }
      }
    }
    console.log('service categories checked', this.Categories);
  }
  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  loadTechData() {
    this.techData = this.global.getFromLocalStorage(this.global.SALON_TECHS)
    // alert(this.techData.length)
    if (this.techData) {
      const firstTech = this.techData[this.techIndex]
      this.selectedTechDetailTapped(firstTech, '', '')
    } else {
      this.global.makeToastOnFailure(AppMessages.msgTechnicianDatamissing)
    }
  }
  calculateAppoCostPrice(service: Services) {

    const _selectedService = service;

    // this.selectedServicesObjects.forEach(element => {
    //   const temp: Services = element
    //   this.totalTime += Number(temp.sser_time)
    //   this.totalPrice += Number(temp.sser_rate)
    // });
    // this.totalPrice = parseFloat(Math.round(this.totalPrice).toFixed(2))
    // return

    if (this.selectedServicesObjects.length === 0) {

      console.log('push rate at zero length', service.sser_rate);
      this.totalPrice += Number(_selectedService.sser_rate)
      this.totalTime += Number(_selectedService.sser_time)
      this.totalPrice = parseFloat(Math.round(this.totalPrice).toFixed(2))
      this.selectedServicesObjects.push(_selectedService)
    } else {

      let objectWasDuplicate = false
      this.selectedServicesObjects.forEach((service: Services, index) => {

        if (service.sser_id === _selectedService.sser_id) {

          let TempTotalTime = 0

          this.selectedServicesObjects.splice(index, 1)
          objectWasDuplicate = true

        }
      });
      if (!objectWasDuplicate) {
        console.log('objectWasDuplicate: is new object ');

        console.log('total price', this.totalPrice, ' _rate', _selectedService.sser_rate);

        this.totalPrice += Number(_selectedService.sser_rate)
        this.totalTime += Number(_selectedService.sser_time)
        this.totalPrice = parseFloat(Math.round(this.totalPrice).toFixed(2))
        this.selectedServicesObjects.push(_selectedService)
      } else {
        console.log('objectWasDuplicate: is new object ');
        this.totalPrice = 0
        let TempTotalTime = 0
        this.selectedServicesObjects.forEach(element => {
          const temp: Services = element
          TempTotalTime += Number(temp.sser_time)
          this.totalPrice += Number(temp.sser_rate)

        });
        this.totalTime = TempTotalTime
        this.totalPrice = parseFloat(Math.round(this.totalPrice).toFixed(2))

      }

    }
    console.log('total price: ', this.totalPrice);
    console.log('total time: ', this.totalTime);

  }
  private saveDataInLocalStorage(res: any) {
    if (res.salon.sal_techs.tech_services === undefined) {

    }
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_OFFERS, res.salon.offers);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_TECHS_SERVICES, res.salon.sal_techs.tech_services);
    this.global.setInLocalStorage(this.global.LOGED_IN_SALON_DATA, res.salon.sal_techs);
    this.global.setInLocalStorage(this.global.SALON_TECHS, res.salon.sal_techs);
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platForm.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 10);
  }
  roundAmount(amount: number | string) {
    return Math.round(Number(amount))
  }
}
