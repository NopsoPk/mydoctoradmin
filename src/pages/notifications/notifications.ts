import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager'
import { AlertController } from 'ionic-angular';
import { Notifications, Tech_appointments, Sal_Techs } from '../../providers/salonInterface'
import { GlobalProvider } from '../../providers/global'

import { DatePipe } from '@angular/common';
import { Events } from 'ionic-angular';
import { SharedataProvider } from '../../providers/sharedata/sharedata'
// import { CallNumber } from '@ionic-native/call-number';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { config } from '../../providers/config';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { SlotsViewPage } from '../slots-view/slots-view';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})

export class NotificationsPage {
  notificationsData: Notifications;
  public upcomingAppointments: Notifications[]
  public archivedAppointments: Notifications[]
  public appointmentsDataSource: Notifications[]
  deviceDatetime = ''
  tempNoti: any[]
  custImageUrl = config.custImg;
  callIcon = 'assets/imgs/call.png'
  defaultcustPic = 'assets/imgs/user.png'
  crossButton = 'assets/imgs/cross.png'

  baseUrl = config.techImg;

  selectedTech: Sal_Techs;
  acceptMessageString = 'Want to accept this Appointment?'
  rejectMessageString = 'Want to reject this Appointment?'
  removeAppoMessageString = 'Want to remove this notification?'
  isBtnAcceptAppointment = false
  isBtnRejectAppointment = false
  isbtnRemoveAppointment = false
  isBtnNewTapped = true;
  public sal_Techs: Sal_Techs[]
  public selectedTechIndex = 0
  appoID = ''
  isValid = false;
  slider: HTMLElement
  SalonID = this.global.getFromLocalStorage(this.global.LOGGED_IN_SALON_ID)
  params = {
    service: btoa('lst_appointments'),
    sal_id: btoa(this.SalonID),
    app_status: btoa('pending'),
    device_datetime: btoa(this.global.getCurrentDeviceDateTime())
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: ServiceManager,

    private alertCtrl: AlertController,
    public global: GlobalProvider,
    public modalCtrl: ModalController,
    public datePipe: DatePipe,
    public appCtrl: App,
    public events: Events,
    private sharedProvider: SharedataProvider,

  ) {
    // body of constructor
    this.sal_Techs = []

    this.upcomingAppointments = []
    this.archivedAppointments = []
    this.needToUpdateData()

  }
  ionViewDidEnter() {

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');

  }
  // this.events.publish('NeedToUpdate', true);
  needToUpdateData() {
    this.events.subscribe('NeedToUpdate', (value) => {
      if (value) {
        this.getNotificationFromServer(false);
      } 

    });

  }
  ionViewWillEnter() {
    this.sharedProvider.makeAppointmentSource.subscribe((res) => {
      if (res) {
        this.getNotificationFromServer(true)
      }
    });
    this.getNotificationFromServer(true)
  }
  ionViewCanEnter() {
  }

  //#region IBACTIONS
  btnNewNotificationTapped() {
    this.isBtnNewTapped = true
    this.slider = document.getElementById('abc');
    this.slider.style.cssFloat = 'left';
    this.appointmentsDataSource = this.upcomingAppointments

  }
  btnArchivedNotificationTapped() {
    this.isBtnNewTapped = false
    this.slider = document.getElementById('abc');
    this.slider.style.cssFloat = 'right';
    this.appointmentsDataSource = this.archivedAppointments
  }

  btnRemoveAppoTapped(appo) {
    this.isbtnRemoveAppointment = true
    this.isBtnAcceptAppointment = false
    this.isBtnRejectAppointment = false
    if (appo.app_id !== undefined) {
      this.appoID = appo.app_id
      this.presentConfirmationDialog(this.removeAppoMessageString)
    } else {
      this.global.makeToastOnFailure(AppMessages.msgMissingAppointmentId)
    }

  }
  btnAcceptTapped(appo) {
    console.log(appo)
    this.isBtnAcceptAppointment = true
    this.isBtnRejectAppointment = false
    this.isbtnRemoveAppointment = false
    if (appo.app_id !== undefined) {
      this.appoID = appo.app_id
      this.presentConfirmationDialog(this.acceptMessageString)
    } else {
      this.global.makeToastOnFailure(AppMessages.msgMissingAppointmentId)
    }
  }

  btnRejectTapped(appo) {
    this.isBtnRejectAppointment = true
    this.isBtnAcceptAppointment = false
    this.isbtnRemoveAppointment = false
    if (appo.app_id !== undefined) {
      this.appoID = appo.app_id
      this.presentConfirmationDialog(this.rejectMessageString)
    } else {
      this.global.makeToastOnFailure(AppMessages.msgMissingAppointmentId)
    }
  }

  btnRescheduleTapped(appo: Tech_appointments) {
    
    let salonTech: Sal_Techs[] = this.global.getFromLocalStorage(this.global.SALON_TECHS);
    if (!salonTech || salonTech.length === 0) {
      return
    }
    this.sal_Techs = salonTech
    this.sal_Techs.forEach((tech, index) => {
      if (tech.tech_id === appo.tech_id) {
        this.selectedTech=tech
        this.selectedTechIndex = index
      }
    });
    this.appCtrl.getRootNav().push(SlotsViewPage, {
      appointment: appo,
      tech_id: appo.tech_id,
      isRescheduleAppointment: true,
      isRecheduleFromNotifications: true,
      techIndex: this.selectedTechIndex,

      selected_Tech: this.selectedTech,

      selected_TechIndex: this.selectedTechIndex,
      appo_TotalTime: 10,
      appo_TotalPrice: appo.app_price,
      appoServicesString: appo.app_services,
      cust_id: appo.cust_id,
      isReschedule: true,

    })
  }

  //#endregion
  // CUSTOME FUNCTIONS

  presentConfirmationDialog(msg: string) {
    const alert = this.alertCtrl.create({
      title: 'Are you sure',
      message: msg,
      buttons: [
        {
          text: 'Yes',
          role: 'Yes',
          handler: () => {
            if (this.isBtnAcceptAppointment) {
              this.acceptAppointment()
            } else if (this.isBtnRejectAppointment) {
              this.rejectAppointment()
            } else if (this.isbtnRemoveAppointment) {
              this.removeAppointment()
            }

            console.log('Yes clicked');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('you canceled this action...');
          }
        }
      ]
    });
    alert.present();
  }

  acceptAppointment() {

    const params = {
      service: btoa('update_app_status'),
      app_id: btoa(this.appoID),
      app_status: btoa('accepted'),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime())
    }
    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((resAppStatus) => {
        console.log('appointment status after accepting appo:')
        this.global.stopProgress()
        this.getNotificationFromServer(true)
        console.log(resAppStatus)
      }, error2 => {
        this.global.stopProgress()
        // this.global.makeToastOnFailure(error2, 'top')

        this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        console.log('something went wrong', error2);
      },
        () => {
          console.log('completion block of accept appointment');
        }
      );
  }
  rejectAppointment() {
    const params = {
      service: btoa('update_app_status'),
      app_id: btoa(this.appoID),
      app_status: btoa('rejected'),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime())
    }
    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((resAppStatus) => {
        console.log(resAppStatus)
        this.global.stopProgress()
        this.getNotificationFromServer(true)
      },
        error1 => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error1, 'top')

          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error1);
        },
        () => {
          console.log('completion block of accept appointment');
        });
  }
  removeAppointment() {

    const params = {
      service: btoa('cancel_app'),
      app_id: btoa(this.appoID),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime())
    }
    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((resAppStatus) => {
        console.log('res remove Appo', resAppStatus);
        this.global.stopProgress()
        this.getNotificationFromServer(true)
        console.log(resAppStatus)
      },
        error5 => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error5, 'top')

          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error5);
        },
        () => {
          console.log('completion block of accept appointment');
        });
  }

  getNotificationFromServer(isShow) {

    console.log('params are...', this.params)
    if (isShow) {
      this.global.showProgress()
    }

    if (this.SalonID == undefined || this.SalonID == null) {
      this.global.makeToastOnFailure(AppMessages.msgSalonIdMissing)
      return
    }
    this.serviceManager.getData(this.params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        console.log('response came');

        this.global.stopProgress()
        if (res.status == 1 && res.msg != undefined) {
          // this.global.makeToastOnFailure(res.msg, 'top')
          console.log('is return back');
          this.upcomingAppointments = []
          this.archivedAppointments = []
          return
        }
        console.log('response for notification ', res);
        this.notificationsData = res;
        this.notificationsData.app_services = res.app_services;
        const appos = res.appointment;
        this.tempNoti = res.appointment;

        this.upcomingAppointments = []
        this.archivedAppointments = []
        appos.forEach(element => {
          const re = /-/gi;
          let appStartTime: string = element.app_start_time;
          appStartTime = appStartTime.replace(re, '/');
          const appStartDate = new Date(appStartTime)
          const CurrentDate = new Date();
          if (appStartDate.getTime() >= CurrentDate.getTime() && element.app_status === this.global.APP_STATUS_PENDING) {
            const abc = element

            this.upcomingAppointments.push(abc)
            console.log('upcoming appo added');

          } else if (element.app_status === this.global.APP_STATUS_PENDING) {
            const ele = element;
            this.archivedAppointments.push(ele)
          }

        });
        // console.log('upcoming appointments');
        // console.log(this.upcomingAppointments);

        // console.log('archived appointments');
        // console.log(this.archivedAppointments);
        // zahid
        this.isBtnNewTapped ? this.appointmentsDataSource = this.upcomingAppointments : this.appointmentsDataSource = this.archivedAppointments;
        this.global.stopProgress()
      },
        error3 => {
          this.global.stopProgress()

          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error3);
        },
      );
  }
  getFromattedTime(appointmentTime) {
    return this.global.getFormattedDate(appointmentTime)
  }
  callToCust(cust_phone: string) {

    window.open('tel:' + cust_phone);

    //   this.callNumber.callNumber(cust_phone, true)
    //     .then(res => console.log('Launched dialer!', res))
    //     .catch(err => console.log('Error launching dialer', err));

  }

}
