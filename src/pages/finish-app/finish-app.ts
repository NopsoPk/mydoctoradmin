import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController, Platform } from 'ionic-angular';
import { NotesPage } from '../notes/notes';
import { ServiceManager } from '../../providers/service-manager';
import { GlobalProvider } from '../../providers/global'
import { Category, Services, Sal_Techs, Tech_appointments, Salon, Offers } from '../../providers/salonInterface';
import * as _ from 'lodash';
import { PopoverController } from 'ionic-angular';
import { OffersPage } from '../offers/offers';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { SharedataProvider } from '../../providers/sharedata/sharedata';
import { AppMessages } from '../../providers/AppMessages/AppMessages';



@IonicPage()
@Component({
  selector: 'page-finish-app',
  templateUrl: 'finish-app.html',
})
export class FinishAppPage {

  appointmentdata: any;
  techServies: Services[] = [];
  tempServies = [];
  servicesSortedByCat = [];
  offerId: string;
  Categories = []
  public selectedServicesObjects: Services[] = []
  TemptotalCost: number;

  checkedServices: Services[] = [];

  lblSelectedOffer: string;

  techID: any;
  appServices = [];
  appointmentSerivicesString: any;
  showmodal: Boolean = false;
  serviceCategories: Category[] = [];
  reviewService: string;
  offers: Offers[] = [];
  offer: Offers;
  offerAmount: number;
  salon: Salon;
  tech_id: any;
  servicechecked = [];
  appCost: number;
  notes: string;
  newArray = [];
  public pageName = 'FinishAppPage';
  public apppointment: Tech_appointments;
  public salonServicesCategories: Category[]
  public salonData: Salon
  public isRescheduleAppointment = false
  public shouldshowOffer = false
  public currencySymbol = ''
  public unregisterBackButtonAction: any;

  // @ViewChild('reviewService') reviewService;


  // ngAfterViewInit() {
  //   // this.reviewService.nativeElement.checked = true;
  // }

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public serviceManager: ServiceManager,
    public global: GlobalProvider,
    private sharedProvider: SharedataProvider,
    public popoverCtrl: PopoverController,
    private platform: Platform,

  ) {

    const $comp = this

    this.techServies = <Services[]>navParams.get('techServices');
    this.tempServies = navParams.get('techServices');

    this.serviceCategories = <Category[]>navParams.get('servicecategories');
    this.salon = navParams.get('salon');
    this.tech_id = navParams.get('tech_id');
    this.apppointment = navParams.get('apppointment');
    this.appCost = parseFloat(this.apppointment.app_price);

    this.appServices = this.apppointment.app_services.split(',');
    this.appointmentSerivicesString = this.apppointment.app_services;

    console.log('salon', this.salon);
    console.log('service', this.appServices);
    // console.log(this.appServices.find(x => x === 'Threading'));

    console.log('this.serviceCategories', this.serviceCategories)
    console.log('this.servicesss', this.techServies)


    // Step:1 Getting Techh ID
    // this.techID = app_tech_id ? app_tech_id : selectedTech.tech_id;
    let techServices = []
    const salonTechs = this.global.getFromLocalStorage(this.global.SALON_TECHS)
    if (salonTechs === undefined && salonTechs === null) {
      console.log('salon techs found nil');
      return;
    }
    // Step:2 Matching Techh ID and Getting Services
    salonTechs.forEach(tech => {
      if (tech.tech_id === this.techID) {
        console.log('tech id matched');

        // techServices = tech.tech_services
        // this.selectedTech = tech
      }
    });

    // Step:3 Removing Duplicate Categories to get Unique Categories
    this.Categories = this.removeDuplicates(this.tempServies, 'ssc_name')

    // Step:4 Sorting Tech services BY Category Name
    this.servicesSortedByCat = []
    this.Categories.forEach(category => {
      const catFromCategory: string = category['ssc_name'];
      this.tempServies.forEach(element => {
        const catFromService: string = element['ssc_name'];
        if (catFromCategory.trim() === catFromService.trim()) {
          const elementNew = element;
          this.servicesSortedByCat.push(elementNew)
        }
      });
    });

    // this.Categories.forEach(function (item) {
    //   const tempservices = $comp.techServies.filter(function (serv) {
    //     return serv.ssc_id === item.ssc_id
    //   })
    //   item.services = tempservices
    // })

    this.servicesSortedByCat.forEach(element => {
      element.checked = false;
      // this.newArray.push(element);
      // data.cheched = false;
      // this.newArray.push(data);
    });
    console.log('this.serviceCategories', this.servicesSortedByCat)

    // for (var i = 0; i < this.serviceCategories.length; i++) {
    for (let j = 0; j < this.appServices.length; j++) {
      // if (this.newArray[i].sser_name === this.appServices[j]) {
      //   this.newArray[i].checked = true;
      // }
      this.servicesSortedByCat.forEach(element => {
        if (element.sser_name === this.appServices[j]) {
          this.checkedServices.push(element);
        }
      });
      // console.log('this.app services', this.appServices[j]);
      // console.log('this.app services', this.servicesSortedByCat);
      let tempcost = this.checkedServices.reduce(function (acc, obj) { return acc + parseFloat(obj.sser_rate); }, 0);
      this.TemptotalCost = (Math.round(Number(tempcost.toFixed(2))));
      // console.log('this.app checked services object', this.checkedServices);
      // console.log('this.app checked services cost', this.TemptotalCost);


      this.checked(this.appServices[j])
    }
    // }
    console.log('checked', this.newArray)

    console.log('service categories', this.serviceCategories);

  }
  checked(value) {
    for (let i = 0; i < this.servicesSortedByCat.length; i++) {
      if (value.trim() === this.servicesSortedByCat[i].sser_name.trim()) {
        this.servicesSortedByCat[i].checked = true
      }
    }
    console.log('service categories checked', this.servicesSortedByCat);
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinishAppPage');

    this.currencySymbol = this.sharedProvider.currencySymbol 
    
    this.salonData = this.global.getFromLocalStorage(this.global.LOGED_IN_SALON_DATA);
    console.log(JSON.stringify(this.salonData));

    if (this.salonData) {
      this.salonServicesCategories = this.salonData.service_categories
      let salonServices: Services[] = this.salonData.services
      this.appCost = 0.0
      this.selectedServicesObjects = []
      // if (this.isRescheduleAppointment) {
      if (this.salonServicesCategories) {
        this.selectedServicesObjects = []
        let appoSevices = this.apppointment.app_services.split(',')

        this.salonServicesCategories.forEach(serviceCategory => {
          let services: Services[] = []


          salonServices.forEach(salService => {
            if (serviceCategory.ssc_id === salService.ssc_id) {
              services.push(salService)
              salService.checked = false
              appoSevices.forEach(appoService => {
                if (appoService.toLowerCase() === salService.sser_name.toLowerCase()) {


                  this.calculateAppoCostPrice(salService)

                  salService.checked = true
                }

              });

            }

            return salService
          });
          return serviceCategory.services = services
        });
      }
      // }
      //  else {


      //    if (this.salonServicesCategories) {

      //      this.salonServicesCategories.forEach(serviceCategory => {
      //        let services: Services[] = []

      //        salonServices.forEach(salService => {
      //          if (serviceCategory.ssc_id === salService.ssc_id) {
      //            services.push(salService)
      //          }
      //          return salService.checked = false
      //        });
      //        return serviceCategory.services = services
      //      });
      //    }
      //  }




    }
    this.calculateValidOffers()

  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  calculateValidOffers() {

    let offerss: Offers[] = this.salon.offers
    this.offers = []
    let myDate = new Date();

    var _dayNumber = myDate.getDay()


    if (_dayNumber === 0) {
      _dayNumber = 7
    }

    offerss.forEach(offer => {
      // console.log(JSON.stringify(offer));
      let offerExpirayDate = new Date(offer.so_expiry)

      let offExInMiliSecond = offerExpirayDate.getTime()
      let currentDateInMilisecond = myDate.getTime()

      let soTime: string = offer.so_time
      let isOffersHoursValid: boolean = false
      if (soTime) {
        isOffersHoursValid = false

        if (soTime.trim().length === 0) {
          isOffersHoursValid = true
        } else {

          let _soTime = offer.so_time.split('&')
          if (_soTime.length === 2) {
            soTime = _soTime[1]

            let so_Start_time = this.global.getStringFromDateWithFormat(new Date(), 'yyyy-MM-dd') + ' ' + _soTime[0].trim() + ":00";
            let so_End_time = this.global.getStringFromDateWithFormat(new Date(), 'yyyy-MM-dd') + ' ' + _soTime[1].trim() + ":00";

            let dateObjSoStartTime = new Date(this.global.CalendarDateFormat(so_Start_time))
            let dateObjSoEndTime = new Date(this.global.CalendarDateFormat(so_End_time))

            let OfferStartingHours = dateObjSoStartTime.getTime()
            let OffersEndingHours = dateObjSoEndTime.getTime()

            let _appoStart = new Date(this.global.CalendarDateFormat(this.apppointment.app_start_time))
            let appoStartTime = _appoStart.getTime()

            isOffersHoursValid = appoStartTime >= OfferStartingHours && OffersEndingHours > appoStartTime

            // console.log(' appoStartTime < OffersEndingHours ' + appoStartTime + ' < ' + OffersEndingHours + ' = ' + (appoStartTime < OffersEndingHours));

          }
        }


      }

      // console.log('currentDateInMilisecond < offExInMiliSecond ' + (currentDateInMilisecond < offExInMiliSecond).toString());
      // console.log('offer.so_days.includes(_dayNumber.toString() '+ offer.so_days.includes(_dayNumber.toString()));
      // console.log('!soTime || isOffersHoursValid ' + !soTime +' || '+ isOffersHoursValid);


      if (offExInMiliSecond >= currentDateInMilisecond && offer.so_days.includes(_dayNumber.toString()) && (!soTime || isOffersHoursValid)) {
        this.offers.push(offer)
      }

    });

    if (!this.offers || this.offers.length === 0) {
      this.shouldshowOffer = false
    } else {
      this.shouldshowOffer = true
    }

  }
  showoffers(event) {

    let popover = this.modalCtrl.create(OffersPage, {
      offers: this.offers,
      // cost: this.appCost,
      // selectedServicesObjects: this.selectedServicesObjects,
      apppointment: this.apppointment,
    }, { cssClass: 'popup-modal' });

    // let notesModal = this.modalCtrl.create(NotesPage, { notes: this.notes, showBackdrop: true, cssClass: 'popup-modal' });

    popover.present({
      // ev: event
    });
    popover.onDidDismiss(data => {



      if (data !== undefined && data !== null && data.so_amount !== null) {
        this.offerAmount = Number(data.so_amount);
        this.offerId = data.so_id;
        this.lblSelectedOffer = data.so_title;
        // this.appCost = this.TemptotalCost;

        console.log('this.TemptotalCost', this.TemptotalCost);

        this.appCost = 0.0
        this.selectedServicesObjects.forEach(ser => {
          this.appCost += Number(ser.sser_rate)
        });

        if (data.sot_id === '1') {

          // alert(Number((this.appCost * (this.offerAmount / 100)).toFixed(2)))

          this.appCost = Number(this.appCost.toFixed(2)) - Number((this.appCost * (this.offerAmount / 100)).toFixed(2));
          if (this.appCost < 0) {
            this.appCost = 0;
          }
          console.log('percentage cost', this.appCost);
        } else if (data.sot_id === '2') {
          this.appCost -= this.offerAmount;
          if (this.appCost < 0) {
            this.appCost = 0;
          }
        }
        console.log('this.offer id =>', this.offerId)
        // this.appointmentRefresh.emit(data);
      }
    });
  }

  removeValue(arr, value) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === value) {
        arr.splice(i, 1);
        break;
      }
    }
    return arr;
  }


  updateCheckedOptions(service, event: any) {
    console.log('service', this.appServices);
    console.log('cost', this.appCost);
    if (event.checked) {
      this.appCost += parseFloat(service.sser_rate);

      this.checkedServices.push(service);
      this.appServices.push(service.sser_name);
      // this.appCost +=  service.sser_rate;

    } else {
      this.appCost -= parseFloat(service.sser_rate);
      if (this.appCost < 0) {
        this.appCost = 0;
      }
      // this.appCost = parseFloat(this.apppointment.app_price) - parseFloat(service.sser_rate);
      this.checkedServices.splice(service, 1);
      this.removeValue(this.appServices, service.sser_name);
    }
    this.appCost = parseFloat(Math.round(this.appCost).toFixed(2));
    // this.servicechecked = service.join(',');

    // this.appServices.concat(this.servicechecked);

    let tempAmount = this.checkedServices.reduce(function (acc, obj) { return acc + parseFloat(obj.sser_rate); }, 0);

    this.TemptotalCost = (Math.round(Number(tempAmount.toFixed(2))));
    // console.log('this.app services all checked service', this.checkedServices);
    // console.log('this.app services all checked service total cost', this.TemptotalCost);
    console.log('service', this.appServices.join(','));
  }

  dismiss() {
    const data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  closemodal() {
    this.showmodal = false;
    this.notes = '';
    console.log('notes', this.notes);
  }

  savenotes() {
    this.showmodal = false;
    console.log('notes', this.notes);
  }


  finishAppointment() {

    if (this.appServices.length === 0 || !this.selectedServicesObjects || this.selectedServicesObjects.length === 0) {
      this.global.makeToastOnFailure(AppMessages.msgSelectService);
      return;
    }
    // console.log('notes', this.notes);
    console.log('services', this.appServices.join(','));
    // console.log('tech', this.tech_id);

    const devicedate: string = new Date().toISOString();
    const paramsFinishApp = {
      service: btoa('update_app_status'),
      app_id: btoa(this.apppointment.app_id),
      app_price_without_offer: btoa(this.apppointment.app_price_without_offer),
      app_status: btoa('finished'),
      tech_id: this.tech_id,
      app_services: btoa(this.appServices.join(',')),
      app_price: btoa(this.appCost.toString()),
      app_est_duration: btoa(this.apppointment.app_est_duration),
      app_notes: btoa(this.notes),
      device_datetime: btoa(devicedate),
      so_id: btoa(this.offerId),
    }
    this.global.showProgress()
    this.serviceManager.getData(paramsFinishApp)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((res) => {
        this.global.stopProgress()
        console.log(res);
        this.viewCtrl.dismiss(res);
      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        });
  }

  presentPrompt() {

    // this.navCtrl.push(NotesPage);
    let notesModal = this.modalCtrl.create(NotesPage, { notes: this.notes, showBackdrop: true, cssClass: 'popup-modal' });

    notesModal.present();

    notesModal.onDidDismiss(data => {
      console.log(data);
      this.notes = data;

    });

    this.showmodal = true;

    console.log('sho modal', this.notes);



    // const alert = this.alertCtrl.create({
    //   title: 'Add Notes',
    //   inputs: [
    //     {
    //       type: 'text',
    //       name: 'notes',
    //       placeholder: 'Write Your notes here',
    //       value: this.notes
    //     }
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       handler: data => {
    //         console.log('Cancel clicked', data);
    //       }
    //     },
    //     {
    //       text: 'Save',
    //       handler: data => {
    //         this.notes = data.notes;
    //         console.log(data.notes);
    //       }
    //     }
    //   ]
    // });
    // alert.present();
  }
  calculateAppoCostPrice(service: Services) {
    this.lblSelectedOffer = null

    const _service = service;

    if (this.selectedServicesObjects.length === 0) {
      this.lblSelectedOffer = null
      console.log('push rate at zero length', service.sser_rate);
      this.selectedServicesObjects.push(_service)
    } else {
      let index = -1;
      let objectWasDuplicate = false
      this.selectedServicesObjects.forEach(serviceInLoop => {
        index += 1;


        if (serviceInLoop.sser_id === _service.sser_id) {
          this.selectedServicesObjects.splice(index, 1)
          objectWasDuplicate = true

          /** 
          console.log('after poping all objects', this.selectedServicesObjects);
          this.appCost = 0
          this.selectedServicesObjects.forEach(element => {
            const temp: Services = element
            
            this.appCost += Number(temp.sser_rate)
          });
          this.appCost = parseFloat(Math.round(this.appCost).toFixed(2))
          */
        }
      });
      if (!objectWasDuplicate) {

        // this.appCost += Number(_service.sser_rate)
        // this.totalTime += Number(_service.sser_time)
        // this.appCost = parseFloat(Math.round(this.appCost).toFixed(2))
        // if (this.totalTime > 60) {
        //   this.totalTime = 60
        // }
        this.selectedServicesObjects.push(_service)
      }

    }

    // console.log('total time: ', this.totalTime);
    if (!this.selectedServicesObjects && this.selectedServicesObjects.length === 0) {
      this.lblSelectedOffer = null
    } else {
      this.appCost = 0.0
      let myAppCost = 0.0
      this.selectedServicesObjects.forEach(ser => {
        myAppCost += Number(ser.sser_rate)
      });
      this.appCost = parseFloat(Math.round(myAppCost).toFixed(2))
    }
  }


  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 10);
  }
  //end custom back button for android


}
