import { NgModule } from '@angular/core';
import { AppointmentDetailComponent } from './appointment-detail/appointment-detail';
import { PrevAppComponent } from './prev-app/prev-app';
import { AppointmentsPageDetailComponent } from './appointments-page-detail/appointments-page-detail';
@NgModule({
	declarations: [AppointmentDetailComponent,
    AppointmentsPageDetailComponent],
	imports: [],
	exports: []
})
export class ComponentsModule {}
