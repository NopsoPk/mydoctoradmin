import { Component, Input, OnInit } from '@angular/core';
import { Salon, Tech_appointments, PrevApp } from '../../providers/salonInterface';
import { ServiceManager } from '../../providers/service-manager';
import { config } from '../../providers/config';
import { GlobalProvider } from '../../providers/global';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
/**
 * Generated class for the PrevAppComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'prev-app',
  templateUrl: 'prev-app.html'
})
export class PrevAppComponent implements OnInit {

  @Input() apppointment: Tech_appointments;
  @Input() salonData: Salon;

  prevAppointments: PrevApp[] = [];
  text: string;
  techimgUrl: string;

  constructor(public global: GlobalProvider, private serviceManager: ServiceManager,) {
    console.log('Hello PrevAppComponent Component');
    this.text = 'Hello World';
  }

  ngOnInit() {
    console.log('Appointmrnt', this.apppointment);

    // let salonData = JSON.parse(localStorage.getItem('salon'));
    console.log('this.global.getCurrentDeviceDateTime()', this.global.getCurrentDeviceDateTime());

    this.techimgUrl = config.techImg;
    const params = {
      service: btoa('get_last_appointment'),
      cust_id: btoa(this.apppointment.cust_id),
      app_id: btoa(this.apppointment.app_id + '|' + this.apppointment.app_start_time),
      sal_id: btoa(this.salonData.sal_id),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
    }
    this.serviceManager.getData(params)
    .retryWhen((err) => {
      return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
              return retryCount;
          }
          else {
              throw (err);
          }
      }, 0).delay(1000)
  })  
  
      .subscribe((res) => {
        if (res.last_appointments !== '') {
          this.prevAppointments = res.last_appointments;
          console.log('prev apppppppppppp', res);
        }
      },
      error => {
        this.global.stopProgress()
        // this.global.makeToastOnFailure(error, 'top')
        this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        console.log('something went wrong', error);
      });
  }
  getFromattedTime(appointmentTime) {
    return this.global.getFormattedTime(appointmentTime)// this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
  }
}
