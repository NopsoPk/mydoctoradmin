import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Platform} from 'ionic-angular';
import { SharedataProvider } from '../../providers/sharedata/sharedata';

/**
 * Generated class for the CalendarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html'
})
export class CalendarComponent implements OnInit {

  @Output() dateSelected = new EventEmitter<Date>()
  @Input() calendarOptions = [];

  public alertShown = false;
  public selectedIndex  = 0;
  public datesArray = [];
  public dateObjsArray = [];
  public months = ["January","February","March","April","May","June",
                  "July","August","September","October","November","December"];
  public days   = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  //private device: Device,
  constructor(public platform: Platform, public sharedProvider: SharedataProvider) {
    // alert('Device UUID is: ' + this.device.model); 
    
    console.log(this.calendarOptions);
  }

  ngOnInit() {
    
    console.log('Options: ', this.calendarOptions);

    for (let i = 0; i < this.calendarOptions['numberOfDays']; i++){
      let date = new Date().getTime() + (i * 86400000);
      var dateObj = {
        date:new Date(date).getDate(),
        day : this.days[new Date(date).getDay()],
        monthAndYear: this.months[new Date(date).getMonth()] +" "+ new Date(date).getFullYear(),
      }
      this.datesArray.push(dateObj);

      // alert(this.calendarOptions['appoDate']);
      if (this.calendarOptions['appoDate'] != 'N/A' && this.calendarOptions['appoDate'] != undefined){
        if (this.calendarOptions['appoDate'].getDate() == new Date(date).getDate() && this.calendarOptions['appoDate'].getMonth() == new Date(date).getMonth()){
            this.selectedIndex = i;
        }else{
          console.log('not it....' + new Date(date) + '====' + this.calendarOptions['appoDate']);
        }
      }

      this.dateObjsArray.push(new Date(date));

    }

  }

  selectDate(date){

  }

  dateClicked(index){
    // this.selectedIndex = index;
    // this.dateSelected.emit(this.dateObjsArray[index]);

    if(this.sharedProvider.showCalenderView){
    
      this.selectedIndex = index;
      this.dateSelected.emit(this.dateObjsArray[index]);
    }
  }

  getMarginLeft(){
    let totalContentWidth = 85 + (25 * 6);
    let difference = this.platform.width() - totalContentWidth;
    return difference / 12 + 'px'
  }

}
