import { Component, OnInit, Input, Output, EventEmitter, trigger, state, style, transition, animate, AfterViewInit } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager';
import { Salon, Sal_Techs, Tech_appointments, Services, Category, PrevApp } from '../../providers/salonInterface';
import { GlobalProvider } from '../../providers/global';
import { Loading } from 'ionic-angular/components/loading/loading';
import { config } from '../../providers/config';
// import * as moment from 'moment';
import { FinishAppPage } from '../../pages/finish-app/finish-app';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { AppointmentsPage } from '../../pages/appointments/appointments';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { PinImageViewerPage } from '../../pages/pin-image-viewer/pin-image-viewer';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.html',
  animations: [
    trigger('expand', [
      state('ActiveGroup', style({ opacity: '1', height: '*' })),
      state('NotActiveGroup', style({ opacity: '0', height: '0', overflow: 'hidden' })),
      transition('ActiveGroup <=> NotActiveGroup', animate('600ms ease-in-out'))
    ]),
  ]
})
export class AppointmentDetailComponent implements OnInit, AfterViewInit {
  @Input() appointment: Tech_appointments;
  @Input() appointments: Tech_appointments[];
  @Input() isAppointmentPage;
  prevAppointments: PrevApp[] = [];
  state: string = 'NotActiveGroup';
  @Input() selectedTech: Sal_Techs;
  @Input() salonData: Salon;
  @Input() techServices: Services[] = [];
  @Output() rescheduleApp = new EventEmitter<Tech_appointments>()
  @Output() appointmentRefresh = new EventEmitter<Tech_appointments>()
  @Output() appointmentTechSelected = new EventEmitter<Sal_Techs>()
  appointmentDetail = '';
  toggleicon = 'add';
  toggle: Boolean = false;
  loading: Loading
  salonServicesCategories: Salon;
  sal_techs: Sal_Techs;
  @Input() serviceCategories: Category[];
  offers: any;
  Serving: Boolean = true;
  calenderview: Boolean = false;
  activetech: Boolean = false;
  selectedTechIndex = 0;
  techimgUrl: string;
  custimgUrl: string;
  constructor(
    private serviceManager: ServiceManager,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public navCtrl: NavController,
    private alertCtrl: AlertController) { }
  ngOnInit() {
    this.techimgUrl = config.techImg;
    this.custimgUrl = config.custImg;
    this.appointments.forEach(element => {
    });
    console.log(this.selectedTech);
    const devicedate: string = new Date().toISOString();
    const params = {
      service: btoa('get_last_appointment'),
      cust_id: btoa(this.appointment.cust_id),
      app_id: btoa(this.appointment.app_id + '|' + this.appointment.app_start_time),
      sal_id: btoa(this.salonData.sal_id),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        if (res.last_appointments !== '') {
          this.prevAppointments = res.last_appointments;
          console.log('prev apppppppppppp', res);
        }
      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
    let lastExpandedAppointmentID = this.global.getFromLocalStorage('selectedAppointmentID')
    if (lastExpandedAppointmentID && lastExpandedAppointmentID === this.appointment.app_id) {
      // this.toggleDetails(lastExpandedAppointmentID)
      // this.toggleDetails(this.appointment.app_id)
      setTimeout(() => {
        // this.toggleDetails(lastExpandedAppointmentID)
      }, 15000);
    }
  }
  ngAfterViewInit() {
  }
  public selectedAppointmentID: string
  toggleDetails(app_id: string, event) {
    if (event) event.stopPropagation();
    this.selectedAppointmentID = app_id
    this.global.setInLocalStorage('selectedAppointmentID', this.selectedAppointmentID)
    this.toggle = !this.toggle;
    this.state = this.state == 'NotActiveGroup' ? 'ActiveGroup' : 'NotActiveGroup';
    // let slideAppointment = document.getElementById('dashappo-'+ app_id).classList.contains('slide-up');
    let slideicon = document.getElementById('toggledash-' + app_id).classList.contains('up');
    console.log(document.getElementById('toggledash-' + app_id));
    if (slideicon) {
      // document.getElementById('dashappo-'+ app_id).classList.remove('slide-up');
      // document.getElementById('dashappo-'+ app_id).classList.add("slide-down");
      document.getElementById('toggledash-' + app_id).classList.remove('up');
      document.getElementById('toggledash-' + app_id).classList.add('down');
    } else {
      // document.getElementById('dashappo-'+ app_id).classList.add('slide-up');
      // document.getElementById('dashappo-'+ app_id).classList.remove("slide-down");
      document.getElementById('toggledash-' + app_id).classList.add('up');
      document.getElementById('toggledash-' + app_id).classList.remove("down");
    }
  }
  callToCust(cust_phone: string) {
    window.open('tel:' + cust_phone);
  }
  startAppointment(appointment: Tech_appointments) {
    // console.log(appointment);
    // const devicedate: string = new Date().toISOString();
    for (let index = 0; index < this.appointments.length; index++) {
      const element = this.appointments[index];
      if (element.app_status === this.global.APP_STATUS_SERVING) {
        // alert('Appointment already in serving');
        this.global.makeToastOnFailure(this.selectedTech.tech_name + AppMessages.msgAppointmentAlreadyInServing)
        return;
      }
    }
    // alert('now start Appointment if not serving');
    const paramsToStartAppoint = {
      service: btoa('update_app_status'),
      app_id: btoa(appointment.app_id),
      app_status: btoa(this.global.APP_STATUS_SERVING),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
    }
    this.global.showProgress();
    this.serviceManager.getData(paramsToStartAppoint)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(data => {
        // console.log(data);
        this.appointmentRefresh.emit(appointment);
        this.global.stopProgress();
        // this.appointmentTechSelected.emit(this.selectedTech);
      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        })
  }
  removeAppointment(appointment: Tech_appointments) {
    // console.log(appointment);
    // this.global.loading.present()
    // const devicedate: string = new Date().toISOString();
    const paramsToDelete = {
      service: btoa('cancel_app'),
      app_id: btoa(appointment.app_id),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
    }
    this.global.showProgress();
    this.serviceManager.getData(paramsToDelete)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        console.log(res);
        if (res.msg === 'Appointment cancelled.' && res.status === '1') {
          this.appointmentRefresh.emit(appointment);
        }
        this.global.stopProgress();
      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
  }
  presentConfirmationDialog(appointment: Tech_appointments) {
    const alert = this.alertCtrl.create({
      title: 'Are you sure',
      message: 'Want to remove this Appointment?',
      buttons: [
        {
          text: 'Yes',
          role: 'Yes',
          handler: () => {
            this.removeAppointment(appointment);
            console.log('Yes clicked');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('you canceled this action...');
          }
        }
      ]
    });
    alert.present();
  }
  rescheduleAppointment(appointment: Tech_appointments) {
    this.rescheduleApp.emit(appointment);
  }
  finishAppointment(salon: Salon, techServices: Services, appointments) {
    // const finishAppModal = this.modalCtrl.create(FinishAppPage,
    //   {
    //     salon: salon,
    //     servicecategories: this.serviceCategories,
    //     techServices: techServices,
    //     tech_id: this.selectedTech.tech_id,
    //     apppointment: this.appointment
    //   }, { showBackdrop: true });
    // finishAppModal.onDidDismiss(data => {
    //   console.log(data);
    //   if (data !== undefined) {
    //     this.appointmentRefresh.emit(data);
    //   }
    // });
    this.finishAppointmentByService(appointments)
    // finishAppModal.present();
  }
  getFromattedTime(appointmentTime) {
    return this.global.getFormattedTime(appointmentTime)// this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
  }
  pinImageTapped(appo) {
    console.log('appoImageTapped', appo);
    debugger
    let imageURL = appo.p_imageUrl
    let modealData = {
      imageURL: imageURL
    }
    this.toggleDetails(appo.app_id, event)
    let mymodal = this.modalCtrl.create(PinImageViewerPage, { data: modealData })
    mymodal.present();
  }
  //finish appointemnt 
  finishAppointmentByService(appointments) {
    const devicedate: string = new Date().toISOString();
    const paramsFinishApp = {
      service: btoa('update_app_status'),
      app_id: btoa(appointments.app_id),
      app_price_without_offer: btoa(appointments.app_price_without_offer),
      app_status: btoa('finished'),
      tech_id: appointments.tech_id,
      app_services: btoa(this.global.getStaticService()),
      app_price: btoa(appointments.app_price),
      app_est_duration: btoa('10'),
      app_notes: btoa(''),
      device_datetime: btoa(devicedate),
      so_id: btoa(''),
    }
    this.global.showProgress()
    this.serviceManager.getData(paramsFinishApp)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        this.global.stopProgress()
        this.appointmentRefresh.emit(res);
      },
        error => {
          this.global.stopProgress()
          // this.global.makeToastOnFailure(error, 'top')
          this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
  }
}
